<?php

namespace Celeritas\Views\CeleritasView\Composers;

class MinifyComposer implements Interfaces\ComposersInterface
{
    /**
     * @param string $html
     *
     * @return string
     */
    public function compose(string $html): string
    {
        return preg_replace(
            array(
                '/ {2,}/',
                '/\t|(?:\r?\n[ \t]*)+/s'
            ),
            array(
                ' ',
                ''
            ),
            $html
        );
    }
}
