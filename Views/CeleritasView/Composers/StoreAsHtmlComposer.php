<?php

namespace Celeritas\Views\CeleritasView\Composers;

class StoreAsHtmlComposer implements Interfaces\ComposersInterface
{
    /**
     * @param string $html
     *
     * @return string
     */
    public function compose(string $html): string
    {
        return $html;
    }
}
