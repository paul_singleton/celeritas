<?php

namespace Celeritas\Views\CeleritasView\Composers\Interfaces;

interface ComposersInterface
{
    /**
     * @param string $html
     *
     * @return string
     */
    public function compose(string $html): string;
}
