<?php

namespace Celeritas\Views\CeleritasView;

class CeleritasView
{
    private array $templateData;
    private array $templatesComposersGlobal = [];
    private array $disabledTemplateComposers = [];

    public function __construct(
        private readonly string $templatesPath
    ) {
        echo '>>>>>>>>>>>';

        // Global substitutions, shared through all the templates
        $this->templateData['shared1'] = 'Something common for all templates';
        $this->templateData['shared2'] = 'Something else common for all templates';

        // Global Composer classes, used for all the templates
        $this->templatesComposersGlobal = [
            Composers\BindCommonDataComposer::class,
            Composers\MinifyComposer::class,
            Composers\StoreAsHtmlComposer::class,
        ];
    }

    public function __destruct()
    {
        echo '<<<<<<<<<<';
    }

    /**
     * @param array $disabledTemplateComposers
     *
     * @return void
     */
    public function disableTemplateComposers(array $disabledTemplateComposers = []): void
    {
        $this->disabledTemplateComposers = $disabledTemplateComposers;
    }

    /**
     * @return array
     */
    private function removeDisabledTemplateComposers(): array
    {
        return array_diff($this->templatesComposersGlobal, $this->disabledTemplateComposers);
    }

    /**
     * @param string $templateFile
     * @param array  ...$templateData
     *
     * @return string
     */
    public function render(string $templateFile, array ...$templateData): string
    {
        $templateData = array_merge(...$templateData);
        $templateData = array_merge($this->templateData, $templateData);

        extract($templateData);

        ob_start();
        require realpath($this->templatesPath . DIRECTORY_SEPARATOR . $templateFile);
        $html = ob_get_clean();

        foreach ($this->removeDisabledTemplateComposers() as $composerClass) {
            $html = (new $composerClass())->compose($html);
        }

        return $html;
    }
}
