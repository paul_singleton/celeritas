<?php

namespace Celeritas\Application\Interfaces;

use Celeritas\Application\ClrsApplication;

interface ClrsBootstrapperInterface
{
    public function bootMeUp(ClrsApplication $app): void;
}
