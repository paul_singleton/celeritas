<?php

namespace Celeritas\Application;

use Ramsey\Uuid\Uuid;
use Celeritas\Configs\ClrsApplicationConfigs;
use Celeritas\Loggers\ClrsApplicationLoggers;
use Celeritas\Http\Router\Abstracts\ClrsApplicationRouterAbstract;
use Celeritas\Http\Controllers\Abstracts\ClrsControllerAbstract;


//use Celeritas\Http\Request\Request;
use Celeritas\Http\Response\Interfaces\ResponseFactoryInterface;


use Celeritas\Http\Janitors\InputVisitors\Interfaces\ClrsInputVisitorInterface;
use Celeritas\Http\Janitors\OutputVisitors\Interfaces\ClrsOutputVisitorInterface;
use Celeritas\Traits\ClrsServiceContainerTrait;

class ClrsApplication
{
    use ClrsServiceContainerTrait;    // We need this trait to produce "singleton", "prototype" and "instance" objects

    public ResponseFactoryInterface $responseFactory;
    public ClrsApplicationConfigs $configs;
    public ClrsApplicationLoggers $loggers;
    public ClrsApplicationRouterAbstract $routers;
    public ClrsControllerAbstract $controllers;

    /**
     * @param string|null  public $uniqueExecutionUuid    Unique request execution ID as a alphanumeric string,
     *                                                    used mainly for logging and debugging.
     */
    public function __construct(
        public ?string $uniqueExecutionUuid = null,
    ) {
        $this->uniqueExecutionUuid ??= Uuid::uuid4();
    }

    /**
     * We pass this method an object, implementing the ClrsBootstrapperInterface interface,
     * an object that performs the bootstrapping actions needed,
     * by running its `bootMeUp()` method.
     *
     * What bootstrapping actions?
     * Those like setting error, exception and shutdown PHP handlers...
     * Creating `$app->loggers` and `$app->configs` as ClrsApplication property objects...
     *
     * As the name suggests, this method is to be used only during the bootstrap phase.
     *
     * @param Interfaces\ClrsBootstrapperInterface $bootstapper
     *
     * @return void
     */
    public function bootstrap(Interfaces\ClrsBootstrapperInterface $bootstapper): void
    {
        $bootstapper->bootMeUp($this);
    }

    /**
     * @param ClrsInputVisitorInterface $visitor
     *
     * @return void
     */
    public function acceptInputJanitorVisitor(
        ClrsInputVisitorInterface $visitor
    ): void {
        $visitor->validateRequest($this);
    }

    /**
     * @param ClrsOutputVisitorInterface $visitor
     *
     * @return void
     */
    public function acceptOutputJanitorVisitor(
        ClrsOutputVisitorInterface $visitor
    ): void {
        $visitor->validateResponse($this);
    }
}
