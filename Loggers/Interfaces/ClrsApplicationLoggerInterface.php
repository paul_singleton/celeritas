<?php

namespace Celeritas\Loggers\Interfaces;

interface ClrsApplicationLoggerInterface
{
    /**
     * Reset all current handlers by:
     * 1. nullify them all
     * 2. re-Push them from the logger specific config file
     *
     * @return self
     */
    public function resetHandlers(): self;

    /**
     * @param array $handlers
     *
     * @return self
     */
    public function setCustomHandlers(array $handlers): self;

    /**
     * Getter of all the handlers as an array.
     *
     * @return array
     */
    public function getHandlers(): array;

    /**
     * Getter of the logging channel.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Setter of the logging channel.
     *
     * @param string $channel
     *
     * @return self
     */
    public function withName(string $channel): self;

    /**
     * @param string $rootKey
     *
     * @return array
     */
    public function getSettings(string $rootKey): array;
}
