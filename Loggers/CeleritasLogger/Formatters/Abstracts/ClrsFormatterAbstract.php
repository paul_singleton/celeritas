<?php

namespace Celeritas\Loggers\CeleritasLogger\Formatters\Abstracts;

abstract class ClrsFormatterAbstract
{
    /**
     * @param string $message
     * @param array  $context
     *
     * @return string
     */
    abstract public function format(string $message, array $context): string;

    /**
     * @param mixed $value
     *
     * @return string
     */
    protected function stringify(mixed $value): string
    {
        return $this->replaceNewlines($this->convertToString($value));
    }

    /**
     * @param string $str
     *
     * @return string
     */
    private function replaceNewlines(string $str): string
    {
        return str_replace(["\r\n", "\r", "\n"], ' ', $str);
    }

    /**
     * @param mixed $data
     *
     * @return string
     */
    private function convertToString(mixed $data): string
    {
        if (null === $data || is_bool($data)) {
            return var_export($data, true);
        }

        if (is_scalar($data)) {
            return (string) $data;
        }

        return $this->toJson($data);
    }

    /**
     * @param mixed $data
     *
     * @return string
     */
    protected function toJson(mixed $data): string
    {
        $json = json_encode($data);

        if (JSON_ERROR_NONE !== json_last_error()) {
            $json = json_last_error_msg();
        }

        return $json;
    }

    /**
     * @param \Throwable $e
     * @param int        $depth
     *
     * @return array
     */
    protected function normalizeException(\Throwable $e, int $depth = 0): array
    {
        $data = [
            'class' => get_class($e),
            'message' => $e->getMessage(),
            'code' => (int) $e->getCode(),
            'file' => basename($e->getFile()) . ':' . $e->getLine(),
        ];

        $trace = $e->getTrace();
        foreach ($trace as $frame) {
            if (isset($frame['file'])) {
                $data['trace'][] = basename($frame['file']) . ':' . $frame['line'];
            }
        }

        if ($previous = $e->getPrevious()) {
            $data['previous'] = $this->normalizeException($previous, $depth + 1);
        }

        return $data;
    }

    /**
     * Interpolates context values into the message placeholders.
     *
     * @param string $message
     * @param array  $context
     *
     * @return string
     */
    public function interpolate(string $message, array $context = []): string
    {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            // check that the value can be cast to string
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }
}
