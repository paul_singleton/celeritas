<?php

namespace Celeritas\Loggers\CeleritasLogger\Formatters;

class ClrsEmailFormatter extends Abstracts\ClrsFormatterAbstract
{
    /**
     * @param string $message
     * @param array  $context
     *
     * @return string
     */
    public function format(string $message, array $context): string
    {
        //...

        $message = strtoupper($message);

        return $message;
    }
}
