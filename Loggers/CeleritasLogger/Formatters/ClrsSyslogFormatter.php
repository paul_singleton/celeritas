<?php

namespace Celeritas\Loggers\CeleritasLogger\Formatters;

class ClrsSyslogFormatter extends Abstracts\ClrsFormatterAbstract
{
    private const SIMPLE_DATE_FORMAT   = 'Y-m-d\TH:i:sP';
    private const SIMPLE_FORMAT = '%uuid% [%datetime%] %channel%.%level_name%: %message%';
    private const LOG_MAX_SIZE = 2048;

    /**
     * @param string $message
     * @param array  $context
     *
     * @return string
     */
    public function format(string $message, array $context): string
    {
        unset($context['exception']);

        $messageFormatted = $this->interpolate($message, $context);

        $messageFormatted = $this->stringify($messageFormatted);

        $trans = [
            '%uuid%' => $context['uuid'],
            '%datetime%' => date(self::SIMPLE_DATE_FORMAT),
            '%channel%' => $context['channel'],
            '%level_name%' => $context['level_name'],
            '%message%' => $messageFormatted,
        ];

        $message = strtr(self::SIMPLE_FORMAT, $trans);

        $message = substr($message, 0, self::LOG_MAX_SIZE);

        return $message;
    }
}
