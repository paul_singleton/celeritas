<?php

namespace Celeritas\Loggers\CeleritasLogger\Formatters;

use Psr\Log\LogLevel;

class ClrsSlackFormatter extends Abstracts\ClrsFormatterAbstract
{
    private const SIMPLE_DATE_FORMAT   = 'D M j G:i:s T Y';
    private const SIMPLE_FORMAT = '%message%  %context%  %extra%';

    /**
     * @param string $message
     * @param array  $context
     *
     * @return string
     */
    public function format(string $message, array $context): string
    {
        // Interpolates context values into the message placeholders.
        $message = $this->interpolate($message, $context);

        $trans = [
            '%message%' => $message,
            '%context%' => 'Some test context...',
            '%extra%' => 'Some test extra...',
        ];

        $message = strtr(self::SIMPLE_FORMAT, $trans);

        $level_name = strtolower($context['level_name']);
        $header_text = substr('Log record from ' . $context['projectName'] ?? '', 0, 150);
        $section_emoji = $this->getSlackEmoji($level_name);
        $section_date = date(self::SIMPLE_DATE_FORMAT);
        $section_message = substr("*Message:*\n $message", 0, 3000);

        $data = [
            "blocks" => [
                [
                    "type" => "header",
                    "text" => [
                        "type" => "plain_text",
                        "text" => $header_text,
                        "emoji" => true
                    ]
                ],
                [
                    "type" => "section",
                    "text" => [
                        "type" => "mrkdwn",
                        "text" => "*UUID:*\n" . $context['uuid'],
                    ]
                ],
                [
                    "type" => "section",
                    "fields" => [
                        [
                            "type" => "mrkdwn",
                            "text" => "*Type:*\n" . $section_emoji . " " . ucfirst($level_name),
                        ],
                        [
                            "type" => "mrkdwn",
                            "text" => "*Channel:*\n" . $context['channel'],
                        ],
                    ]
                ],
                [
                    "type" => "section",
                    "text" => [
                        "type" => "mrkdwn",
                        "text" => "*When:*\n" . $section_date,
                    ]
                ],
                [
                    "type" => "divider"
                ],
                [
                    "type" => "section",
                    "text" => [
                        "type" => "mrkdwn",
                        "text" => $section_message,
                    ]
                ]
            ]
        ];

        return json_encode($data);
    }

    /**
     * https://gist.github.com/rxaviers/7360908
     *
     * @param string $level
     *
     * @return string
     */
    private function getSlackEmoji(string $level): string
    {
        return match ($level) {
            LogLevel::EMERGENCY => ':boom:',
            LogLevel::ALERT     => ':rotating_light:',
            LogLevel::CRITICAL  => ':fire:',
            LogLevel::ERROR     => ':x:',
            LogLevel::WARNING   => ':heavy_exclamation_mark:',
            LogLevel::NOTICE    => ':point_right:',
            LogLevel::INFO      => ':information_source:',
            LogLevel::DEBUG     => ':microscope:',
        };
    }
}
