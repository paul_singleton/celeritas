<?php

namespace Celeritas\Loggers\CeleritasLogger\Formatters;

class ClrsDefaultFormatter extends Abstracts\ClrsFormatterAbstract
{
    private const SIMPLE_DATE_FORMAT   = 'Y-m-d\TH:i:sP';
    private const SIMPLE_FORMAT = '%uuid% [%datetime%] %channel%.%level_name%: %message% %context% %exception%' . PHP_EOL;
    private const MAX_NORMALIZE_DEPTH = 9;

    /**
     * @param string $message
     * @param array  $context
     *
     * @return string
     */
    public function format(string $message, array $context): string
    {
        $exceptionFormatted = '';
        if (!empty($context['exception']) && ($context['exception'] instanceof \Throwable)) {
            $exceptionFormatted = $this->normalizeException($context['exception'], self::MAX_NORMALIZE_DEPTH);
            $exceptionFormatted = $this->stringify($exceptionFormatted);
        }

        $messageFormatted = $this->interpolate($message, $context);

        $messageFormatted = $this->stringify($messageFormatted);

        unset($context['exception']);
        $contextFormatted = $this->stringify($context);

        $trans = [
            '%uuid%' => $context['uuid'],
            '%datetime%' => date(self::SIMPLE_DATE_FORMAT),
            '%channel%' => $context['channel'],
            '%level_name%' => $context['level_name'],
            '%message%' => $messageFormatted,
            '%context%' => $contextFormatted,
            '%exception%' => $exceptionFormatted,
        ];

        return strtr(self::SIMPLE_FORMAT, $trans);
    }
}
