<?php

namespace Celeritas\Loggers\CeleritasLogger;

use Celeritas\Application\ClrsApplication;
use Celeritas\Loggers\{
    Interfaces\ClrsApplicationLoggerInterface,
    Traits\ClrsApplicationLoggerTrait,
};
use Psr\Log\AbstractLogger;

class ClrsCeleritasLogger extends AbstractLogger implements ClrsApplicationLoggerInterface
{
    use ClrsApplicationLoggerTrait;

    private readonly array $loggerSettings;

    private string $channel;
    private array $handlers = [];
    private array $logHandlers = [];

    public function __construct(
        private readonly ClrsApplication $app
    ) {
        // Read the corresponding config file
        $this->loggerSettings = $this->app->configs->all('celeritas_logging');

        $this->channel = $this->loggerSettings['default_channel'];

        $this->resetHandlers();
    }

    /**
     * Set handlers, replacing all existing ones.
     *
     * @param Closure $handlers
     */
    public function setCustomHandlers(array $handlers): self
    {
        $this->logHandlers = [];

        foreach (array_reverse($handlers) as $handler) {
            $this->pushHandler($handler);
        }

        return $this;
    }

    /**
     * @param Closure $handler
     *
     * @return self
     */
    private function pushHandler(\Closure $handler): self
    {
        array_unshift($this->logHandlers, $handler);

        return $this;
    }

    /**
     * @return Closure[]
     */
    public function getHandlers(): array
    {
        return $this->logHandlers;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->channel;
    }

    /**
     * @param string $channel
     *
     * @return self
     */
    public function withName(string $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @param mixed              $level
     * @param string|\Stringable $message
     * @param array              $context
     *
     * @return void
     */
    public function log(mixed $level, string|\Stringable $message, array $context = []): void
    {
        // Set used channel property back to default so that it is
        // not preserved throgh the different calls
        $context['channel'] = $this->getName();
        $this->channel = $this->loggerSettings['default_channel'];

        // Set used handlers property back to default so that it is
        // not preserved throgh the different calls.
        $handlersToUse = $this->getHandlers();

        $context['uuid'] = $this->app->uniqueExecutionUuid;
        $context['projectName'] = $this->app->configs->get('CLRTS_APP_NAME');
        $context['level_name'] = $level;

        foreach ($handlersToUse as $handlerObject) {
            // Perform the logging
            $handlerObject()
                ->setFormatter($this->loggerSettings['default_formatter'])
                ->performLogging($message, $context);
        }
    }
}
