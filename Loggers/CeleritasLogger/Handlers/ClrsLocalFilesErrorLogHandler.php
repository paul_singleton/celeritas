<?php

namespace Celeritas\Loggers\CeleritasLogger\Handlers;

class ClrsLocalFilesErrorLogHandler extends Abstracts\ClrsHandlersAbstract
{
    private readonly string $logfilepath;

    /**
     * @param string $message
     * @param array  $context
     *
     * @return bool
     */
    public function performLogging(string $message, array $context = []): bool
    {
        // Format the message, using the default or custom formatter
        $message = $this->formatter->format($message, $context);

        return error_log($message, 3, $this->logfilepath);
    }

    /**
     * @param string $logfilepath
     *
     * @return self
     */
    public function setLogFile(string $logfilepath): self
    {
        $this->logfilepath = realpath($logfilepath);

        return $this;
    }
}
