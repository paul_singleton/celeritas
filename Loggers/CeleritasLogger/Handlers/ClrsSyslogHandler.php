<?php

namespace Celeritas\Loggers\CeleritasLogger\Handlers;

use Psr\Log\LogLevel;

class ClrsSyslogHandler extends Abstracts\ClrsHandlersAbstract
{
    private const LOG_PREFIX = 'Celeritas';

    /**
     * @param string $message
     * @param array  $context
     *
     * @return bool
     */
    public function performLogging(string $message, array $context = []): bool
    {
        // Format the message, using the default or custom formatter
        $message = $this->formatter->format($message, $context);

        $level_name = strtolower($context['level_name']);

        $priority = $this->getSyslogCode($level_name);

        /**
         * If there is an error while sending data to the system logger, write
         * directly to the system console AND include PID with each message
         * what type of program is logging the message? Generic user-level messages
         */
        $flags = LOG_CONS | LOG_PID;
        $facility = LOG_USER;

        if (openlog(self::LOG_PREFIX, $flags, $facility)) {
            syslog($priority, $message);
            return closelog();
        }

        return false;
    }

    /**
     * @param string $level
     *
     * @return string
     */
    private function getSyslogCode(string $level): string
    {
        return match ($level) {
            LogLevel::EMERGENCY => LOG_EMERG,
            LogLevel::ALERT     => LOG_ALERT,
            LogLevel::CRITICAL  => LOG_CRIT,
            LogLevel::ERROR     => LOG_ERR,
            LogLevel::WARNING   => LOG_WARNING,
            LogLevel::NOTICE    => LOG_NOTICE,
            LogLevel::INFO      => LOG_INFO,
            LogLevel::DEBUG     => LOG_DEBUG,
        };
    }
}
