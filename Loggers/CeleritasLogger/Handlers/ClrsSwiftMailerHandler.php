<?php

namespace Celeritas\Loggers\CeleritasLogger\Handlers;

class ClrsSwiftMailerHandler extends Abstracts\ClrsHandlersAbstract
{
    private readonly string $hostName;
    private readonly string $userName;
    private readonly string $passWord;
    private readonly string $encryption;

    private readonly array $toEmails;
    private readonly array $fromEmail;

    private readonly string $subject;

    private readonly int $portNum;

    /**
     * @param string $message
     * @param array  $context
     *
     * @return bool
     */
    public function performLogging(string $message, array $context = []): bool
    {
        // Format the message, using the default or custom formatter
        $message = $this->formatter->format($message, $context);

        // Create the Transport
        $transport = (new \Swift_SmtpTransport())
            ->setHost($this->hostName)
            ->setPort($this->portNum)
            ->setEncryption($this->encryption)
            ->setUsername($this->userName)
            ->setPassword($this->passWord);

        // Create the Mailer using your created Transport
        $mailer = new \Swift_Mailer($transport);

        // Create a message
        $swfMessage = (new \Swift_Message($this->subject))
            ->setTo($this->toEmails)
            ->setFrom($this->fromEmail)
            ->setContentType('text/html')
            ->setBody($message);

        // Send the message
        $result = $mailer->send($swfMessage);

        return $result > 0;
    }

    /**
     * @param string $hostName
     *
     * @return self
     */
    public function setHost(string $hostName): self
    {
        $this->hostName = $hostName;

        return $this;
    }

    /**
     * @param int $portNum
     *
     * @return self
     */
    public function setPort(int $portNum): self
    {
        $this->portNum = $portNum;

        return $this;
    }

    /**
     * @param string $encryption
     *
     * @return self
     */
    public function setEncryption(string $encryption): self
    {
        $this->encryption = $encryption;

        return $this;
    }

    /**
     * @param string $userName
     *
     * @return self
     */
    public function setUsername(string $userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * @param string $passWord
     *
     * @return self
     */
    public function setPassword(string $passWord): self
    {
        $this->passWord = $passWord;

        return $this;
    }

    /**
     * @param array $toEmails
     *
     * @return self
     */
    public function setTo(array $toEmails): self
    {
        $this->toEmails = $toEmails;

        return $this;
    }

    /**
     * @param array $fromEmail
     *
     * @return self
     */
    public function setFrom(array $fromEmail): self
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    /**
     * @param string $subject
     *
     * @return self
     */
    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }
}
