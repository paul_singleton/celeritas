<?php

namespace Celeritas\Loggers\CeleritasLogger\Handlers;

class ClrsEmailLogHandler extends Abstracts\ClrsHandlersAbstract
{
    private readonly string $toEmails;
    private readonly string $fromEmail;
    private readonly string $subject;

    /**
     * @param string $message
     * @param array  $context
     *
     * @return bool
     */
    public function performLogging(string $message, array $context = []): bool
    {
        // Format the message, using the default or custom formatter
        $message = $this->formatter->format($message, $context);

        return error_log(
            $message,
            1,
            $this->toEmails,
            $this->composeExtraHeaders()
        );
    }

    public function setTo(string $toEmails): self
    {
        $this->toEmails = $toEmails;

        return $this;
    }

    public function setFrom(string $fromEmail): self
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    private function composeExtraHeaders(): string
    {
        $headers[] = 'Subject: ' . $this->subject;
        $headers[] = 'From: ' . $this->fromEmail;

        return implode(PHP_EOL, $headers);
    }
}
