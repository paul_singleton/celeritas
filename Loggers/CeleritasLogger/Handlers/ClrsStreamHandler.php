<?php

namespace Celeritas\Loggers\CeleritasLogger\Handlers;

class ClrsStreamHandler extends Abstracts\ClrsHandlersAbstract
{
    private readonly string $with;

    /**
     * @param string $message
     * @param array  $context
     *
     * @return bool
     */
    public function performLogging(string $message, array $context = []): bool
    {
        // Format the message, using the default or custom formatter
        $message = $this->formatter->format($message, $context);

        $fh = fopen($this->with, 'a');
        fwrite($fh, $message);
        return fclose($fh);
    }

    /**
     * @param string $with
     *
     * @return self
     */
    public function setWith(string $with): self
    {
        $this->with = $with;

        return $this;
    }
}
