<?php

namespace Celeritas\Loggers\CeleritasLogger\Handlers\Abstracts;

use Celeritas\Loggers\CeleritasLogger\Formatters\Abstracts\ClrsFormatterAbstract;

abstract class ClrsHandlersAbstract
{
    protected ClrsFormatterAbstract $formatter;

    /**
     * @param ClrsFormatterAbstract $formatter
     *
     * @return self
     */
    public function setFormatter(ClrsFormatterAbstract $formatter): self
    {
        if (empty($this->formatter)) {
            $this->formatter = $formatter;
        }
        return $this;
    }

    /**
     * @param string $message
     * @param array  $context
     *
     * @return bool
     */
    abstract public function performLogging(string $message, array $context): bool;
}
