<?php

namespace Celeritas\Loggers\CeleritasLogger\Handlers;

class ClrsLocalFilesAccessLogHandler extends Abstracts\ClrsHandlersAbstract
{
    private readonly string $logFilePath;

    /**
     * @param string $message
     * @param array  $context
     *
     * @return bool
     */
    public function performLogging(string $message, array $context = []): bool
    {
        // Format the message, using the default or custom formatter
        $message = $this->formatter->format($message, $context);

        return error_log($message, 3, $this->logFilePath);
    }

    /**
     * @param string $logFilePath
     *
     * @return self
     */
    public function setLogFile(string $logFilePath): self
    {
        $this->logFilePath = realpath($logFilePath);

        return $this;
    }
}
