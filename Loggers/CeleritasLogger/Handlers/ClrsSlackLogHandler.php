<?php

namespace Celeritas\Loggers\CeleritasLogger\Handlers;

class ClrsSlackLogHandler extends Abstracts\ClrsHandlersAbstract
{
    private readonly string $url;

    /**
     * @param string $message
     * @param array  $context
     *
     * @return bool
     */
    public function performLogging(string $message, array $context = []): bool
    {
        // Format the message, using the default or custom formatter
        $message = $this->formatter->format($message, $context);

        // https://api.slack.com/apps
        // https://api.slack.com/apps/A02LSTR9NSD
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($message)
            ]
        );

        $result = curl_exec($ch);
        curl_close($ch);

        // https://api.slack.com/messaging/webhooks#handling_errors
        return $result === 'ok';
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setChannelUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
