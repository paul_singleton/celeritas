<?php

namespace Celeritas\Loggers\Traits;

trait ClrsApplicationLoggerTrait
{
    /**
     * @param string $rootKey
     *
     * @return array
     */
    public function getSettings(string $rootKey): array
    {
        return empty($rootKey) ? $this->loggerSettings : $this->loggerSettings[$rootKey];
    }

    /**
     * Reset all current handlers by:
     * 1. nullify them all
     * 2. re-Push them from the logger specific config file
     *
     * @return self
     */
    public function resetHandlers(): self
    {
        // If "default_handlers" is present - get only the default handlers
        $defaultHandlers = array_filter(
            $this->loggerSettings['handlers'],
            fn (string $key): bool =>
                in_array($key, $this->loggerSettings['default_handlers'] ?? array_keys($this->loggerSettings['handlers'])),
            ARRAY_FILTER_USE_KEY
        );

        $this
            ->setCustomHandlers([])
            ->setCustomHandlers($defaultHandlers);

        return $this;
    }
}
