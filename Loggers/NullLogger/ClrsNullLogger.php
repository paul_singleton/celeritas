<?php

namespace Celeritas\Loggers\NullLogger;

use Celeritas\Loggers\Interfaces\ClrsApplicationLoggerInterface;

/**
 * Logging should always be optional.
 * This logger can be used to avoid conditional log calls by implementing the "Null object Design Pattern"
 * and avoids littering your code with `if ($this->app->logger) { }` blocks.
 */
class ClrsNullLogger extends \Psr\Log\NullLogger implements ClrsApplicationLoggerInterface
{
    /**
     * @return self
     */
    public function resetHandlers(): self
    {
        return $this;
    }

    /**
     * @param array $handlers
     */
    public function setCustomHandlers(array $handlers): self
    {
        return $this;
    }

    /**
     * @return array
     */
    public function getHandlers(): array
    {
        return [];
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function withName(string $name): self
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return '';
    }

    /**
     * @param string $rootKey
     *
     * @return array
     */
    public function getSettings(string $rootKey): array
    {
        return [];
    }
}
