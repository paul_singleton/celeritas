<?php

namespace Celeritas\Loggers;

use Psr\Log\LoggerInterface;
use Celeritas\Application\ClrsApplication;
use Celeritas\Traits\ClrsServiceContainerTrait;

class ClrsApplicationLoggers
{
    use ClrsServiceContainerTrait {
        instance as private instanceTrait;
        singleton as private singletonTrait;
        prototype as private prototypeTrait;
    }

    private LoggerInterface $defaultLogger;

    private string $defaultChannel;
    private ?string $dynamicChannel = null;

    private array $defaultHandlers;
    private ?array $dynamicHandlersKeys = null;

    public function __construct(
        private readonly ClrsApplication $app
    ) {
    }

    /**
     * Gets the logger name from the config data.
     *
     * @return string|null
     */
    public function defaultLoggerCode(): ?string
    {
        return $this->app->configs->get('CLRTS_LOGGER_CLASS');
    }

    /**
     * Dynamically set the logger as a pristine, new object, everytime we need one.
     *
     * @param string $code
     *
     * @return self
     */
    public function instance(string $code = null): self
    {
        $defaultLogger = $this->instanceTrait(code: $code);

        $this->setDefaultLogger($defaultLogger);

        return $this;
    }

    /**
     * Dynamically set the logger as a singleton object, everytime we need it.
     *
     * @param string $code
     *
     * @return self
     */
    public function singleton(string $code = null): self
    {
        $defaultLogger = $this->singletonTrait(code: $code);

        $this->setDefaultLogger($defaultLogger);

        return $this;
    }

    /**
     * Dynamically set the logger as a cloned from a previously instantiated prototype,
     * everytime we need one.
     *
     * @param string $code
     *
     * @return self
     */
    public function prototype(string $code = null): self
    {
        $defaultLogger = $this->prototypeTrait(code: $code);

        $this->setDefaultLogger($defaultLogger);

        return $this;
    }

    /**
     * @param LoggerInterface|null $defaultLogger
     *
     * @return void
     */
    private function setDefaultLogger(?LoggerInterface $defaultLogger): void
    {
        $this->defaultLogger = $defaultLogger ?: new \Celeritas\Loggers\NullLogger\ClrsNullLogger();

        // Logger level channel name
        $this->defaultChannel = $this->defaultLogger
            ->getName();

        // Logger level handlers
        $this->defaultHandlers = $this->defaultLogger
            ->resetHandlers()
            ->getHandlers();
    }

    /**
     * Dynamically set the needed channel.
     *
     * @param string $channel
     *
     * @return self
     */
    public function channel(string $channel): self
    {
        $this->dynamicChannel = $channel;

        return $this;
    }

    /**
     * Dynamically set the needed handlers.
     *
     * @param array $handlersKeys
     *
     * @return self
     */
    public function handlers(array $handlersKeys): self
    {
        $this->dynamicHandlersKeys = $handlersKeys;

        return $this;
    }

    /**
     * The ninth method from the psr-3 specification.
     * Route the call to the "calIt()" method.
     *
     * @param string             $level     One of the Psr\Log\LogLevel levels name
     * @param string|\Stringable $message
     * @param array              $context
     *
     * @return void
     */
    public function log(string $level, string|\Stringable $message, array $context = []): void
    {
        $this->logIt($level, $message, $context);
    }

    /**
     * Majic PHP method, used to intercept the eight psr-3 log methods call.
     * Route the call to the "calIt()" method.
     *
     * @param string $name
     * @param array $arguments
     *
     * @return void
     */
    public function __call(string $name, array $arguments): void
    {
        $this->logIt($name, ...$arguments);
    }

    /**
     * Called from both "log()" and "__call()".
     * Calls the other eight psr-3 log methods.
     * Here we must have all the necessary values (channel, handlers...),
     * given to the particular logger library.
     *
     * @param string $level
     * @param string|\Stringable $message
     * @param array $context
     *
     * @return void
     */
    private function logIt(string $level, string|\Stringable $message, array $context = []): void
    {
        // Is this log method switched on or off?
        if ($this->isSwitchedOn($level) === false) {
            return;
        }

        $this->setDynamics();

        $this->defaultLogger
            ->withName($this->dynamicChannel ?? $this->defaultChannel)
            ->$level($message, $context);

        $this->nullifyDynamics();
    }

    /**
     * Per cpecific logger library - set its handlers /if such given/ in $this->dynamicHandlersKeys
     *
     * @return void
     */
    private function setDynamics(): void
    {
        // Do we want to dynamically set handlers?
        if (empty($this->dynamicHandlersKeys)) {
            return;
        }

        // Filter out only the ones needed, using their array keys
        $dynamicHandlers = array_filter(
            $this->defaultLogger->getSettings('handlers'),
            fn (string $key): bool =>
                in_array($key, $this->dynamicHandlersKeys),
            ARRAY_FILTER_USE_KEY
        );

        // Nullify and repopulate the logger handlers
        $this->defaultLogger
            ->setCustomHandlers([])
            ->setCustomHandlers($dynamicHandlers);
    }

    /**
     * Nullify the dynamically set handlers and restores the ones, that are per library.
     *
     * @return void
     */
    private function nullifyDynamics(): void
    {
        // Nullify the dynamic channel and handler keys
        $this->dynamicChannel = null;
        $this->dynamicHandlersKeys = null;

        // Get the default logging library, set in the config object
        $this->singleton($this->defaultLoggerCode());

        // Logger level channel name
        $this->defaultChannel = $this->defaultLogger
            ->getName();

        // Logger level handlers
        $this->defaultHandlers = $this->defaultLogger
            ->resetHandlers()
            ->getHandlers();
    }

    /**
     * Each specific logiing method may be swithched on/off globally.
     *
     * @param string $level
     *
     * @return bool
     */
    private function isSwitchedOn(string $level): bool
    {
        return (bool) $this->app->configs->get('CLRTS_lOGGER/' . $level);
    }
}
