<?php

namespace Celeritas\Http\Message\Abstracts;

use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;

abstract class ClrsMessageAbstract implements MessageInterface
{
    protected string $protocolVersion = '1.1';
    protected array $headers = [];
    protected StreamInterface $body;

    protected function getProtocolVersion(): string
    {
        if (empty($_SERVER['SERVER_PROTOCOL'])) {
            return $this->protocolVersion;
        }

        $protocolVersionArray = explode('/', $_SERVER['SERVER_PROTOCOL']);

        return $protocolVersionArray[1] ?? $this->protocolVersion;
    }

    protected function withProtocolVersion(string $protocolVersion): static
    {
        if ($this->protocolVersion === $protocolVersion) {
            return $this;
        }

        $new = clone $this;
        $new->protocolVersion = $protocolVersion;
        return $new;
    }

    protected function getHeaders(): array
    {
        $allHeaders = getallheaders();

        $allHeaders = array_map(
            fn (string $value): array => explode(',', $value),
            $allHeaders
        );

        array_walk_recursive($allHeaders, 'trim');

        return $allHeaders;
    }

    protected function hasHeader(string $name): bool
    {
        $name = strtolower($name);

        $headers = array_change_key_case($this->headers, CASE_LOWER);

        return !empty($headers[$name]);
    }

    protected function getHeader(string $name): array
    {
        $name = strtolower($name);

        $headers = array_change_key_case($this->headers, CASE_LOWER);

        return $headers[$name] ?? [];
    }

    protected function getHeaderLine(string $name): string
    {
        return implode(', ', $this->getHeader($name));
    }

    protected function withHeader(string $name, string|array $value): static
    {
        $this->assertHeader($name);

        $new = clone $this;

        $new->headers[$name] = is_string($value)
            ? trim($value)
            : $this->normalizeHeaderValue($value);

        return $new;
    }

    protected function withAddedHeader(string $name, string|array $value): static
    {
        $this->assertHeader($name);

        $value = $this->normalizeHeaderValue($value);

        $new = clone $this;

        if ($this->hasHeader($name) === false) {
            $new->headers[$name] = $value;
            return $new;
        }

        $currentHeaderValues = $this->getHeader($name);

        $new->headers[$name] = array_merge($currentHeaderValues, $value);

        return $new;
    }

    protected function withoutHeader(string $name): static
    {
        if ($this->hasHeader($name) === false) {
            return $this;
        }

        $new = clone $this;
        unset($new->headers[$name]);
        return $new;
    }

    protected function getBody(): StreamInterface
    {
        if (!$this->body) {
            $this->body = new ClrsStream('php://input');
        }

        return $this->body;
    }

    protected function withBody(StreamInterface $body): static
    {
        if ($body === $this->body) {
            return $this;
        }

        $new = clone $this;
        $new->body = $body;
        return $new;
    }

    /**
     * @see https://tools.ietf.org/html/rfc7230#section-3.2
     *
     * @param string $headerName
     */
    private function assertHeader(string $headerName): void
    {
        preg_match('/^[a-zA-Z0-9\'`#$%&*+.^_|~!-]+$/', $headerName)
            or throw new \InvalidArgumentException(
                sprintf(
                    '"%s" is not valid header name',
                    $headerName
                )
            );
    }

    /**
     * Trims whitespace from the header values.
     *
     * Spaces and tabs ought to be excluded by parsers when extracting the field value from a header field.
     *
     * header-field = field-name ":" OWS field-value OWS
     * OWS          = *( SP / HTAB )
     *
     * @param array $values Header values
     *
     * @return array Trimmed header values
     *
     * @see https://tools.ietf.org/html/rfc7230#section-3.2.4
     *
     * @param array $headerValues
     *
     * @return array
     */
    private function normalizeHeaderValue(array $headerValues): array
    {
        (count($headerValues) >= 0)
            or throw new \InvalidArgumentException('Header value can not be an empty array.');

        return array_map(function (string $value) {
            return trim($value, " \t");
        }, array_values($headerValues));
    }
}
