<?php

namespace Celeritas\Http\Message;

use Celeritas\Http\Message\ClrsRequest;
use Psr\Http\Message\ServerRequestInterface;

class ClrsServerRequest extends ClrsRequest implements ServerRequestInterface
{
    public function __construct()
    {
        $method = $_SERVER['REQUEST_METHOD'] ?? 'GET';
        $headers = getallheaders();
        //$uri = self::getUriFromGlobals();
        //$body = new CachingStream(new LazyOpenStream('php://input', 'r+'));
        //$protocol = isset($_SERVER['SERVER_PROTOCOL']) ? str_replace('HTTP/', '', $_SERVER['SERVER_PROTOCOL']) : '1.1';

        parent::__construct($method, $uri);
    }

    public function getServerParams(): array
    {}

    public function getCookieParams(): array
    {}

    public function withCookieParams(array $cookies): static
    {}

    public function getQueryParams(): array
    {}

    public function withQueryParams(array $query): static
    {}

    public function getUploadedFiles(): array
    {}

    public function withUploadedFiles(array $uploadedFiles): static
    {}

    public function getParsedBody(): null|array|object
    {}

    public function withParsedBody(null|array|object $data): static
    {}

    public function getAttributes(): array
    {}

    public function getAttribute(string $name, mixed $default = null): mixed
    {}

    public function withAttribute(string $name, mixed $value): static
    {}

    public function withoutAttribute(string $name): static
    {}
}
