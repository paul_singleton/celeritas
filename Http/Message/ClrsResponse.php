<?php

namespace Celeritas\Http\Message;

use Celeritas\Http\Message\Abstracts\ClrsMessageAbstract;
use Psr\Http\Message\ResponseInterface;

class ClrsResponse extends ClrsMessageAbstract implements ResponseInterface
{
    public function getStatusCode()
    {}

    public function withStatus(int $code, string $reasonPhrase = '')
    {}

    public function getReasonPhrase()
    {}
}
