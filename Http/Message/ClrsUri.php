<?php

namespace Celeritas\Http\Message;

use Psr\Http\Message\UriInterface;

class ClrsUri implements UriInterface
{
    /**
     * Unreserved characters for use in a regex.
     *
     * @link https://tools.ietf.org/html/rfc3986#section-2.3
     */
    private const CHAR_UNRESERVED = 'a-zA-Z0-9_\-\.~';

    /**
     * Sub-delims for use in a regex.
     *
     * @link https://tools.ietf.org/html/rfc3986#section-2.2
     */
    private const CHAR_SUB_DELIMS = '!\$&\'\(\)\*\+,;=';
    //private const QUERY_SEPARATORS_REPLACEMENT = ['=' => '%3D', '&' => '%26'];

    private readonly string $scheme;
    private readonly string $host;
    private ?int $port = null;
    private readonly string $path;
    private readonly string $query;
    private readonly string $fragment;
    private readonly string $userInfo;

    public function __construct(string $uri)
    {
        $parts = parse_url($uri);

        $this->scheme = strtolower($parts['scheme'] ?? '');
        $this->host = strtolower($parts['host'] ?? '');
        $this->port = $this->filterPort($parts['port'] ?? null);
        $this->path = $this->filterPath($parts['path'] ?? '');
        $this->query = $this->filterQueryAndFragment($parts['query'] ?? '');
        $this->fragment = $this->filterQueryAndFragment($parts['fragment'] ?? '');
        $this->userInfo = $this->filterUserInfoComponent($parts['user'] ?? '');
    }

    public function __toString(): string
    {
        return $this->composeComponents(
            $this->scheme,
            $this->getAuthority(),
            $this->path,
            $this->query,
            $this->fragment
        );
    }


    public function getScheme(): string
    {
        return $this->scheme;
    }

    public function withScheme(string $scheme): static
    {
        if ($this->scheme === $scheme) {
            return $this;
        }

        $new = clone $this;
        $new->scheme = $scheme;
        return $new;
    }


    public function getAuthority(): string
    {
        $authority = $this->host;
        if ($this->userInfo !== '') {
            $authority = $this->userInfo . '@' . $authority;
        }

        if ($this->port !== null) {
            $authority .= ':' . $this->port;
        }

        return $authority;
    }


    public function getUserInfo(): string
    {
        return $this->userInfo;
    }

    public function withUserInfo(string $user, ?string $password = null): static
    {
        $userInfo = $this->filterUserInfoComponent($user);
        if ($password !== null) {
            $userInfo .= ':' . $this->filterUserInfoComponent($password);
        }

        if ($this->userInfo === $userInfo) {
            return $this;
        }

        $new = clone $this;
        $new->userInfo = $userInfo;
        return $new;
    }


    public function getHost(): string
    {
        return $this->host;
    }

    public function withHost(string $host): static
    {
        $host = strtolower($host);

        if ($this->host === $host) {
            return $this;
        }

        $new = clone $this;
        $new->host = $host;
        return $new;
    }


    public function getPort(): ?int
    {
        return $this->port;
    }

    public function withPort(?int $port = null): static
    {
        $port = $this->filterPort($port);

        if ($this->port === $port) {
            return $this;
        }

        $new = clone $this;
        $new->port = $port;
        return $new;
    }


    public function getPath(): string
    {
        return $this->path;
    }

    public function withPath(string $path): static
    {
        $path = $this->filterPath($path);

        if ($this->path === $path) {
            return $this;
        }

        $new = clone $this;
        $new->path = $path;
        return $new;
    }


    public function getQuery(): string
    {
        return $this->query;
    }

    public function withQuery(string $query): static
    {
        $query = $this->filterQueryAndFragment($query);

        if ($this->query === $query) {
            return $this;
        }

        $new = clone $this;
        $new->query = $query;
        return $new;
    }


    public function getFragment(): string
    {
        return $this->fragment;
    }

    public function withFragment(string $fragment): static
    {
        $fragment = $this->filterQueryAndFragment($fragment);

        if ($this->fragment === $fragment) {
            return $this;
        }

        $new = clone $this;
        $new->fragment = $fragment;
        return $new;
    }


    /**
     * @param int|null $port
     *
     * @throws \InvalidArgumentException If the port is invalid.
     *
     * @return int|null
     */
    private function filterPort(?int $port): ?int
    {
        if ($port === null) {
            return null;
        }

        if (0 > $port || 0xffff < $port) {
            throw new \InvalidArgumentException(
                sprintf('Invalid port: %d. Must be between 0 and 65535', $port)
            );
        }

        return $port;
    }

    /**
     * Filters the path of a URI
     *
     * @param string $path
     *
     * @return string
     */
    private function filterPath(string $path): string
    {
        return preg_replace_callback(
            '/(?:[^' . self::CHAR_UNRESERVED . self::CHAR_SUB_DELIMS . '%:@\/]++|%(?![A-Fa-f0-9]{2}))/',
            [$this, 'rawurlencodeMatchZero'],
            $path
        );
    }

    /**
     * Filters the query string or fragment of a URI.
     *
     * @param string $str
     *
     * @return string
     */
    private function filterQueryAndFragment(string $str): string
    {
        return preg_replace_callback(
            '/(?:[^' . self::CHAR_UNRESERVED . self::CHAR_SUB_DELIMS . '%:@\/\?]++|%(?![A-Fa-f0-9]{2}))/',
            [$this, 'rawurlencodeMatchZero'],
            $str
        );
    }

    /**
     * @param string $component
     *
     * @return string
     */
    private function filterUserInfoComponent(string $component): string
    {
        return preg_replace_callback(
            '/(?:[^%' . self::CHAR_UNRESERVED . self::CHAR_SUB_DELIMS . ']+|%(?![A-Fa-f0-9]{2}))/',
            [$this, 'rawurlencodeMatchZero'],
            $component
        );
    }

    /**
     * @param array $match
     *
     * @return string
     */
    private function rawurlencodeMatchZero(array $match): string
    {
        return rawurlencode($match[0]);
    }

    /**
     * Composes a URI reference string from its various components.
     *
     * Usually this method does not need to be called manually but instead is used indirectly via
     * `Psr\Http\Message\UriInterface::__toString`.
     *
     * PSR-7 UriInterface treats an empty component the same as a missing component as
     * getQuery(), getFragment() etc. always return a string. This explains the slight
     * difference to RFC 3986 Section 5.3.
     *
     * Another adjustment is that the authority separator is added even when the authority is missing/empty
     * for the "file" scheme. This is because PHP stream functions like `file_get_contents` only work with
     * `file:///myfile` but not with `file:/myfile` although they are equivalent according to RFC 3986. But
     * `file:///` is the more common syntax for the file scheme anyway (Chrome for example redirects to
     * that format).
     *
     * @link https://tools.ietf.org/html/rfc3986#section-5.3
     */
    private function composeComponents(?string $scheme, ?string $authority, string $path, ?string $query, ?string $fragment): string
    {
        $uri = '';

        // weak type checks to also accept null until we can add scalar type hints
        if ($scheme != '') {
            $uri .= $scheme . ':';
        }

        if ($authority != ''|| $scheme === 'file') {
            $uri .= '//' . $authority;
        }

        $uri .= $path;

        if ($query != '') {
            $uri .= '?' . $query;
        }

        if ($fragment != '') {
            $uri .= '#' . $fragment;
        }

        return $uri;
    }
}
