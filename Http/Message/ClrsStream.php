<?php

namespace Celeritas\Http\Message;

use Psr\Http\Message\StreamInterface;

class ClrsStream implements StreamInterface
{
    /**
     * @see http://php.net/manual/function.fopen.php
     * @see http://php.net/manual/en/function.gzopen.php
     */
    private const READABLE_MODES = '/r|a\+|ab\+|w\+|wb\+|x\+|xb\+|c\+|cb\+/';
    private const WRITABLE_MODES = '/a|w|r\+|rb\+|rw|x|c/';

    private readonly resource $handle;

    private readonly bool $seekable;
    private readonly bool $readable;
    private readonly bool $writable;

    public function __construct(string $filename, string $mode = 'r', bool $use_include_path = false, ?resource $context = null)
    {
        $this->handle = fopen($filename, $mode, $use_include_path, $context)
            or throw new \RuntimeException(sprintf(
                'Unable to open "%s" using mode "%s"',
                $filename,
                $mode,
            ));

        $meta = stream_get_meta_data($this->stream);

        $this->seekable = $meta['seekable'];
        $this->readable = (bool) preg_match(self::READABLE_MODES, $meta['mode']);
        $this->writable = (bool) preg_match(self::WRITABLE_MODES, $meta['mode']);
    }

    /**
     * Closes the stream when destructed
     */
    public function __destruct()
    {
        $this->close();
    }

    public function __toString(): string
    {}

    public function close(): void
    {
        is_resource($this->handle) && fclose($this->handle);

        $this->detach();
    }

    public function detach(): ?resource
    {}

    public function getSize(): ?int
    {}

    public function tell(): int
    {}

    public function eof(): bool
    {}

    public function isReadable(): bool
    {
        return $this->readable;
    }

    public function isWritable(): bool
    {
        return $this->writable;
    }

    public function isSeekable(): bool
    {
        return $this->seekable;
    }

    public function seek(int $offset, int $whence = \SEEK_SET): void
    {}

    public function rewind(): void
    {}

    public function write(string $string): int
    {}

    public function read(int $length): string
    {}

    public function getContents(): string
    {
        if (!isset($this->handle)) {
            throw new \RuntimeException('Stream is detached');
        }

        return stream_get_contents($this->handle)
            or throw new \RuntimeException('Unable to read stream contents');
    }

    public function getMetadata(string $key = null): array|mixed|null
    {}
}
