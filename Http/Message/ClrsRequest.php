<?php

namespace Celeritas\Http\Message;

use Celeritas\Http\Message\Abstracts\ClrsMessageAbstract;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

class ClrsRequest extends ClrsMessageAbstract implements RequestInterface
{
    protected string $requestTarget;
    protected string $method;
    protected UriInterface $uri;

    protected function __construct(
        string $method,
        UriInterface $uri,
    ) {
        $this->assertMethod($method);

        $this->method = strtoupper($method);
        $this->uri = $uri;
    }

    public function getRequestTarget(): string
    {
        if ($this->requestTarget !== null) {
            return $this->requestTarget;
        }

        $target = $this->uri->getPath();
        if ($target === '') {
            $target = '/';
        }
        if ($this->uri->getQuery() != '') {
            $target .= '?' . $this->uri->getQuery();
        }

        return $target;
    }

    public function withRequestTarget(string $requestTarget): static
    {
        $this->assertRequestTarget($requestTarget);

        $new = clone $this;
        $new->requestTarget = $requestTarget;
        return $new;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function withMethod(string $method): static
    {
        $this->assertMethod($method);

        $new = clone $this;
        $new->method = strtoupper($method);
        return $new;
    }

    public function getUri(): UriInterface
    {}

    public function withUri(UriInterface $uri, bool $preserveHost = false): static
    {}

    /**
     * @param string $requestTarget
     */
    private function assertRequestTarget(string $requestTarget): void
    {
        !preg_match('#\s#', $requestTarget)
            or throw new InvalidArgumentException('Invalid request target provided; cannot contain whitespace');
    }

    /**
     * @param string $method
     */
    private function assertMethod(string $method): void
    {
        (trim($method) !== '')
            or throw new \InvalidArgumentException('HTTP method must be a non-empty string.');
    }
}
