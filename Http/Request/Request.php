<?php

namespace Celeritas\Http\Request;

class Request
{
    private readonly array $requestServerData;
    private readonly array $requestGetData;
    private readonly array $requestPostData;
    private readonly array $requestCookieData;
    private readonly array $requestHeaders;

    public function __construct()
    {
        $this->requestServerData = $_SERVER;
        $this->requestGetData    = $_GET;
        $this->requestPostData   = $_POST;
        $this->requestCookieData = $_COOKIE;
        $this->requestHeaders    = getallheaders();
    }

    /**
     * @param string|null $field
     *
     * @return string
     */
    public function getServerValue(string $field = null): string|array|null
    {
        if (is_null($field)) {
            return $this->requestServerData;
        }
        return $this->requestServerData[$field] ?? null;
    }

    /**
     * @param string|null $field
     *
     * @return string
     */
    public function getGetValue(string $field = null): string|array|null
    {
        if (is_null($field)) {
            return $this->requestGetData;
        }
        return $this->requestGetData[$field] ?? null;
    }

    /**
     * @param string|null $field
     *
     * @return string
     */
    public function getPostValue(string $field = null): string|array|null
    {
        if (is_null($field)) {
            return $this->requestPostData;
        }
        return $this->requestPostData[$field];
    }

    /**
     * @param string|null $field
     *
     * @return string
     */
    public function getHeaderValue(string $field = null): string|array|null
    {
        if (is_null($field)) {
            return $this->requestHeaders;
        }
        return $this->requestHeaders[$field];
    }

    /**
     * @param array $newGetArgs
     *
     * @return array
     */
    public function combineGetParams(array $newGetArgs = []): array
    {
        $currentGetsNPosts = [
            'g' => $this->getGetValue(),
            'p' => $this->getPostValue()
        ];

        $requestOrder = ini_get('request_order') ?: ini_get('variables_order');
        $requestOrder = preg_replace('#[^cgp]#', '', strtolower($requestOrder)) ?: 'gp';

        $orderedGPArgs = [];

        foreach (str_split($requestOrder) as $order) {
            $orderedGPArgs[] = $currentGetsNPosts[$order];
        }

        $orderedGPArgs = array_merge(...$orderedGPArgs);

        $mergedGetArgs = array_merge($orderedGPArgs, $newGetArgs);

        $mergedGetArgs = array_filter(
            $mergedGetArgs,
            function (?string $val): bool {
                return !is_null($val);
            }
        );

        return $mergedGetArgs;
    }

    /**
     * Get HTTP request method (SERVER['REQUEST_METHOD']) from
     * the Request object given.
     *
     * @return string
     */
    public function getRequestMethod(): string
    {
        return $this->getServerValue('REQUEST_METHOD');
    }

    /**
     * Get HTTP request URI (SERVER['REQUEST_URI']) from the Request object given
     * and remove the query string (if such) and trim off the "/" (if such).
     *
     * @return string
     */
    public function getRequestPath(): string
    {
        $requestUri = $this->getServerValue('REQUEST_URI');
        $requestPath = strtok($requestUri, '?');
        $requestPath = rawurldecode($requestPath);
        $requestPath = trim(trim(trim($requestPath), '/'));
        return $requestPath;
    }
}
