<?php

namespace Celeritas\Http\Request\Enums;

enum ClrsHTTPMethodsEnum
{
    case get;
    case post;
    case put;
    case delete;
}
