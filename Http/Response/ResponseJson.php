<?php

namespace Celeritas\Http\Response;

class ResponseJson extends Response
{
    // Common JSON headers used for all the HTML responses
    protected array $responseHeaders = [
        'Some-common-JSON-header-name' => 'some common json 123',
        'Content-Type' => 'application/json; charset=utf-8',
    ];

    private readonly array $jsonData;

    /**
     * @param array $jsonData
     *
     * @return self
     */
    public function setJsonData(array $jsonData): self
    {
        $this->jsonData = $jsonData;

        return $this;
    }

    /**
     * Encode the response JSON, add it as a response body
     * and return int to be spit out.
     *
     * @return self
     */
    public function render(): self
    {
        $json = json_encode($this->jsonData);

        $this->setResponseBody($json);

        return $this;
    }
}
