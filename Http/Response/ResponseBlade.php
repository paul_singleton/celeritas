<?php

namespace Celeritas\Http\Response;

use Celeritas\Configs\ClrsApplicationConfigs;
use Jenssegers\Blade\Blade;

class ResponseBlade extends Response
{
    // Common HTML headers used for all the HTML responses
    protected array $responseHeaders = [
        'Some-common-BLADE-header-name' => 'some common blade header 333',
        'Content-Type' => 'text/html; charset=UTF-8',
    ];

    private readonly string $htmlTemplate;
    private readonly array $htmlData;

    private readonly Blade $blade;

    public function __construct(
        private readonly ClrsApplicationConfigs $configs
    ) {
        $templatesPath = realpath($this->configs->get('/view/templates/'));
        $compiledPath = realpath($this->configs->get('/view/compiled/') . '/blade');

        $this->blade = new Blade($templatesPath, $compiledPath);
    }

    /**
     * @param string $htmlTemplate
     *
     * @return self
     */
    public function setHtmlTemplate(string $htmlTemplate): self
    {
        $this->htmlTemplate = $htmlTemplate;

        return $this;
    }

    /**
     * @param array $htmlData
     *
     * @return self
     */
    public function setHtmlData(array $htmlData): self
    {
        $this->htmlData = $htmlData;

        return $this;
    }

    /**
     * Compile the response HTML, add it as a response body
     * and return int to be spit out.
     *
     * @return self
     */
    public function render(): self
    {
        $html = $this->blade->render($this->htmlTemplate, $this->htmlData);

        $this->setResponseBody($html);

        return $this;
    }
}
