<?php

namespace Celeritas\Http\Response\Interfaces;

interface ResponseInterface
{
    /**
     * @param array $headers
     *
     * @return self
     */
    public function setResponseHeaders(array $headers): self;

    /**
     * @param int $code
     *
     * @return self
     */
    public function setResponseCode(int $code): self;

    /**
     * @param string $body
     *
     * @return self
     */
    public function setResponseBody(string $body): self;
}
