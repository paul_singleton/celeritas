<?php

namespace Celeritas\Http\Response;

use Celeritas\Configs\ClrsApplicationConfigs;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class ResponseTwig extends Response
{
    // Common HTML headers used for all the HTML responses
    protected array $responseHeaders = [
        'Some-common-TWIG-header-name' => 'some common twig 333',
        'Content-Type' => 'text/html; charset=UTF-8',
    ];

    private readonly string $htmlTemplate;
    private readonly array $htmlData;

    private readonly Environment $twig;

    public function __construct(
        private readonly ClrsApplicationConfigs $configs
    ) {
        $templatesPath = realpath($this->configs->get('/view/templates/'));
        $compiledPath = realpath($this->configs->get('/view/compiled/') . '/twig');

        $loader = new FilesystemLoader($templatesPath);
        $this->twig = new Environment(
            $loader,
            [
                'cache' => $pathfile = $compiledPath,
            ]
        );
    }

    /**
     * @param string $htmlTemplate
     *
     * @return self
     */
    public function setHtmlTemplate(string $htmlTemplate): self
    {
        $this->htmlTemplate = $htmlTemplate;

        return $this;
    }

    /**
     * @param array $htmlData
     *
     * @return self
     */
    public function setHtmlData(array $htmlData): self
    {
        $this->htmlData = $htmlData;

        return $this;
    }

    /**
     * Compile the response HTML, add it as a response body
     * and return int to be spit out.
     *
     * @return self
     */
    public function render(): self
    {
        $html = $this->twig->render($this->htmlTemplate, $this->htmlData);

        $this->setResponseBody($html);

        return $this;
    }
}
