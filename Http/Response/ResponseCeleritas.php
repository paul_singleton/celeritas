<?php

namespace Celeritas\Http\Response;

use Celeritas\Configs\ClrsApplicationConfigs;
use Celeritas\Views\CeleritasView\CeleritasView;

class ResponseCeleritas extends Response
{
    // Common HTML headers used for all the HTML responses
    protected array $responseHeaders = [
        'Some-common-CELERITAS-header-name' => 'some common html 666',
        'Content-Type' => 'text/html; charset=UTF-8',
    ];

    private readonly string $htmlTemplate;
    private readonly array $htmlData;

    private readonly CeleritasView $celeritasView;

    public function __construct(
        private readonly ClrsApplicationConfigs $configs
    ) {
        $templatesPath = realpath($this->configs->get('/view/templates/'));

        $this->celeritasView = new CeleritasView($templatesPath);
    }

    /**
     * @param string $htmlTemplate
     *
     * @return self
     */
    public function setHtmlTemplate(string $htmlTemplate): self
    {
        $this->htmlTemplate = $htmlTemplate;

        return $this;
    }

    /**
     * @param array $htmlData
     *
     * @return self
     */
    public function setHtmlData(array $htmlData): self
    {
        $this->htmlData = $htmlData;

        return $this;
    }

    /**
     * @return self
     */
    public function render(): self
    {
        $html = $this->celeritasView->render($this->htmlTemplate, $this->htmlData);

        $this->setResponseBody($html);

        return $this;
    }
}
