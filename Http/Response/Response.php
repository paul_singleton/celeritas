<?php

namespace Celeritas\Http\Response;

class Response implements Interfaces\ResponseInterface, \Stringable
{
    // Common HTTP headers used for all the responses
    protected array $responseHeaders = [
        'Some-common-HTML-header-name' => 'some common html 111',
        'Some-common-header-name' => 'some common header value',
    ];

    // Lets consider that the page requested is by default found
    protected int $responseCode = 200;

    // Lets consider the default response to be an empty string
    protected string $responseBody = '';

    // Response cookies
    protected array $responseCookies = [];

    /**
     * @param array $headers
     *
     * @return self
     */
    public function setResponseHeaders(array $headers): self
    {
        $this->responseHeaders = array_merge($this->responseHeaders, $headers);

        return $this;
    }

    /**
     * @param int $code
     *
     * @return self
     */
    public function setResponseCode(int $code): self
    {
        $this->responseCode = $code;

        return $this;
    }

    /**
     * @param string $body
     *
     * @return self
     */
    public function setResponseBody(string $body): self
    {
        $this->responseBody = $body;

        return $this;
    }

    /**
     * Attaching cookies to the response.
     *
     * @param array $cookies
     *
     * @return self
     */
    public function setResponseCookies(array $cookies): self
    {
        $this->responseCookies = $cookies;

        return $this;
    }

    /**
     * Expiring cookies early in order to remove it.
     *
     * @param array $cookies
     *
     * @return self
     */
    public function removeResponseCookies(array $cookies): self
    {
        //...
        return $this;
    }

    /**
     * The download method may be used to generate a response that forces the user's browser
     * to download the file at the given path.
     *
     * @param string      $pathToFile
     * @param string|null $name
     * @param array|null  $headers
     *
     * @return never
     */
    public function downloadFile(string $pathToFile, string $name = null, array $headers = null): never
    {
        //...
        die;
    }

    /**
     * In index.php we echo the response out.
     *
     * @return string
     */
    public function __toString(): string
    {
        http_response_code($this->responseCode);

        foreach ($this->responseHeaders as $key => $val) {
            header($key . ': ' . $val, true);
        }

        return $this->responseBody;
    }

    /**
     * @param string $url
     * @param array  $getData
     *
     * @return never
     */
    public function redirectGet(string $url, array $getData = []): never
    {
        $qs = '';
        if (!empty($getData)) {
            $qs = http_build_query($getData);
            $qs = '?' . $qs;
        }

        header('Location: ' . $url . $qs);
        die;
    }

    /**
     * @param string $url
     * @param array  $postData
     *
     * @return never
     */
    public function redirectPost(string $url, array $postData): never
    {
        $ch = curl_init($url);

        if (!empty($postData)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        }

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $resp = curl_exec($ch);

        if ($resp === false) {
            $c_error = curl_error($ch);
            curl_close($ch);
            throw new \RuntimeException('Curl error: ' . $c_error);
        }

        curl_close($ch);
        die;
    }
}
