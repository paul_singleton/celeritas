<?php

namespace Celeritas\Http\Router\Interfaces;

use Celeritas\Application\ClrsApplication;
use Celeritas\Http\Response\Interfaces\ResponseInterface;

interface ClrsRouterLinksInterface
{
    public function run(ClrsApplication $app): ResponseInterface;
}
