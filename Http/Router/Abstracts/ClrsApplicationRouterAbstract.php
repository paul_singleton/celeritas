<?php

namespace Celeritas\Http\Router\Abstracts;

use Celeritas\Http\Router;
use Celeritas\Http\Router\Abstracts;
use Celeritas\Http\Request\Request;
use Celeritas\Http\Response\Interfaces\ResponseInterface;

abstract class ClrsApplicationRouterAbstract
{
    protected array $standardRoutes = [];
    protected array $callbackRoutes = [];
    protected ?Router\ClrsFallbackRouterLink $fallbackRoute = null;

    private Abstracts\ClrsRouterLinkAbstract $matchedRoute;

    /**
     * @return ResponseInterface
     */
    public function runMatchedRoute(): ResponseInterface
    {
        return $this->matchedRoute->run($this->app);
    }

    /**
     * @param string|null $httpMethod
     * @param string $pattern
     * @param string $controllerClass
     * @param \Closure $controllerAction
     * @param string|null $name
     * @param array|null $hardcodedArgs
     *
     * @return void
     */
    public function standardRouterLink(
        string $pattern,
        string $controllerClass,
        \Closure $controllerAction,
        string $httpMethod = null,
        string $name = null,
        array $hardcodedArgs = null
    ): void
    {
        $this->standardRoutes[] = new Router\ClrsStandardRouterLink($httpMethod, $pattern, $controllerClass, $controllerAction, $name, $hardcodedArgs);
    }

    /**
     * @param string|null $httpMethod
     * @param string $pattern
     * @param \Closure $callback
     *
     * @return void
     */
    public function callbackRouterLink(
        string $pattern,
        \Closure $callback,
        string $httpMethod = null,
    ): void
    {
        $this->callbackRoutes[] = new Router\ClrsCallbackRouterLink($httpMethod, $pattern, $callback);
    }

    /**
     * @param string $controllerClass
     * @param \Closure $controllerAction
     *
     * @return void
     */
    public function fallbackRouterLink(
        string $controllerClass,
        \Closure $controllerAction
    ): void
    {
        $this->fallbackRoute = new Router\ClrsFallbackRouterLink($controllerClass, $controllerAction);
    }

    /**
     * @param Request $httpRequest
     *
     * @return self
     */
    public function runRouting(Request $httpRequest): self
    {
        // Get the SERVER['REQUEST_URI'], remove the query string (if such) and trim off the "/" (if such)
        $requestPath = $httpRequest->getRequestPath();

        // Get the SERVER['REQUEST_METHOD']
        $requestMethod = $httpRequest->getRequestMethod();

        // Iterate through all the routes and match the needed one,
        // filtering out all that are only standard ones.
        $matchedStandardRoute = $this->matchStandardRoute($this->standardRoutes, $requestPath, $requestMethod);
        if (!empty($matchedStandardRoute)) {
            $this->matchedRoute = $matchedStandardRoute;
            return $this;
        }

        // Iterate through all the routes and match the needed one,
        // filtering out all that are only callback ones.
        $matchedCallbackRoute = $this->matchCallbackRoute($this->callbackRoutes, $requestPath, $requestMethod);
        if (!empty($matchedCallbackRoute)) {
            $this->matchedRoute = $matchedCallbackRoute;
            return $this;
        }        

        // No matching route found, fallbacking...
        // ...unless fallback route is set!
        if (empty($this->fallbackRoute)) {
            throw new \RuntimeException('We\'ve went through all the routes and did not find any match. There is no fallback route too.');
        }
        $this->matchedRoute = $this->fallbackRoute;

        return $this;
    }

    /**
     * Iterate through all the routes and match the needed one,
     * filtering out all that are only standard ones.
     *
     * @param array $standardRoutes
     * @param string $requestPath
     * @param string $requestMethod
     *
     * @return Router\ClrsStandardRouterLink|null
     */
    private function matchStandardRoute(array $standardRoutes, string $requestPath, string $requestMethod): ?Router\ClrsStandardRouterLink
    {
        $matchedStandardRoutes = array_filter(
            $standardRoutes,
            fn (Router\ClrsStandardRouterLink $standardRoute) =>
                ($standardRoute instanceof Router\ClrsStandardRouterLink) &&
                !is_null($this->matchMethodAndPath($standardRoute, $requestPath, $requestMethod))
        );

        // array_filter() returns array, right? But we need only the array element.
        return array_pop($matchedStandardRoutes);
    }

    /**
     * Iterate through all the routes and match the needed one,
     * filtering out all that are only callback ones.
     *
     * @param array $callbackRoutes
     * @param string $requestPath
     * @param string $requestMethod
     *
     * @return Router\ClrsCallbackRouterLink|null
     */
    private function matchCallbackRoute(array $callbackRoutes, string $requestPath, string $requestMethod): ?Router\ClrsCallbackRouterLink
    {
        $matchedCallbackRoutes = array_filter(
            $callbackRoutes,
            fn (Router\ClrsCallbackRouterLink $callbackRoute) =>
                ($callbackRoute instanceof Router\ClrsCallbackRouterLink) &&
                !is_null($this->matchMethodAndPath($callbackRoute, $requestPath, $requestMethod))
        );

        // array_filter() returns array, right? But we need only the array element.
        return array_pop($matchedCallbackRoutes);
    }

    /**
     * Match which Controller->action is set to handle this particular request.
     *
     * @param Abstracts\ClrsRouterLinkAbstract $givenRoute
     * @param string $requestPath
     * @param string $requestMethod
     *
     * @return Abstracts\ClrsRouterLinkAbstract|null
     */
    private function matchMethodAndPath(
        Abstracts\ClrsRouterLinkAbstract $givenRoute,
        string $requestPath,
        string $requestMethod,
    ): ?Abstracts\ClrsRouterLinkAbstract
    {
        // Match the request method to the route method or to "any" method
        if ($this->matchHttpMethod($givenRoute, $requestMethod) === false) {
            return null;
        }

        // Get the route URI and trim the "/"s off
        $routePattern = $this->getRouteUri($givenRoute);

        // Preg match the request and the route URI pattern
        $matchedUri = (bool) preg_match("/^$routePattern$/ui", $requestPath, $matches, PREG_UNMATCHED_AS_NULL);

        array_shift($matches);

        // Corresponding route found /with matches in the pattern/!
        if ($matchedUri && !empty($matches)) {
            // Route arguments, hardcoded as an array.
            $hardcodedArguments = (array) $givenRoute->getRouteHardcodedArgs();

            // Merge the pattern args, passed in the URL and those, set statically.
            // Those from the URI (preg_match() matches) will be first.
            $allArguments = array_merge($matches, $hardcodedArguments);

            $givenRoute->setControllerActionArgs(...array_values($allArguments));

            return $givenRoute;
        }

        // Corresponding route found /with no mathes in the pattern/!
        return ($matchedUri) ? $givenRoute : null;
    }

    /**
     * Get route uri pattern from the Router\ClrsStandardRouterLink object given
     * and trim the "/"s off.
     *
     * @param Abstracts\ClrsRouterLinkAbstract $routerLink
     *
     * @return string
     */
    private function getRouteUri(Abstracts\ClrsRouterLinkAbstract $givenRoute): string
    {
        $routeUri = $givenRoute->getRoutePattern();
        $routeUri = trim(trim(trim($routeUri), '/'));
        $routeUri = str_replace('/', '\/', $routeUri);
        return $routeUri;
    }    

    /**
     * Note that if for the given route no HTTP method is set,
     * it will accespt all globaly permitted HTTP methods.
     *
     * @param Abstracts\ClrsRouterLinkAbstract $givenRoute
     * @param string $requestMethod
     *
     * @return bool
     */
    private function matchHttpMethod(Abstracts\ClrsRouterLinkAbstract $givenRoute, string $requestMethod): bool
    {
        return empty($givenRoute->getRouteMethod())
                 || ($givenRoute->getRouteMethod() === strtolower($requestMethod));
    }
}
