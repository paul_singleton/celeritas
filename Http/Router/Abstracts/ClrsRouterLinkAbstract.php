<?php

namespace Celeritas\Http\Router\Abstracts;

use Celeritas\Http\Router\Interfaces;

abstract class ClrsRouterLinkAbstract implements Interfaces\ClrsRouterLinksInterface
{
    protected array $controllerArgs = [];

    /**
     * @return string|null
     */
    public function getRouteMethod(): ?string
    {
        return $this->httpMethod;
    }

    /**
     * @return string
     */
    public function getRoutePattern(): string
    {
        return $this->pattern;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controllerClass;
    }

    /**
     * @return \Closure
     */
    public function getAction(): \Closure
    {
        return $this->controllerAction;
    }

    /**
     * @return string|null
     */
    public function getRouteName(): ?string
    {
        return $this->name;
    }

    /**
     * @return array|null
     */
    public function getRouteHardcodedArgs(): ?array
    {
        return $this->hardcodedArgs;
    }

    /**
     * @param mixed ...$controllerArgs
     *
     * @return void
     */
    public function setControllerActionArgs(...$controllerArgs): void
    {
        $this->controllerArgs = $controllerArgs;
    }

    /**
     * @return array
     */
    public function getControllerActionArgs(): array
    {
        return $this->controllerArgs;
    }
}
