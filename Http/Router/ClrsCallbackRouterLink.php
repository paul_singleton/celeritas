<?php

namespace Celeritas\Http\Router;

use Celeritas\Application\ClrsApplication;
use Celeritas\Http\Response\Interfaces\ResponseInterface;

class ClrsCallbackRouterLink extends Abstracts\ClrsRouterLinkAbstract
{
    public function __construct(
        protected readonly ?string $httpMethod = null,
        protected readonly string $pattern,
        protected readonly \Closure $callback,
        protected readonly ?string $name = null,
        protected readonly ?array $hardcodedArgs = null,
    ) {
    }

    /**
     * @return \Closure
     */
    public function getCallback(): \Closure
    {
        return $this->callback;
    }

    /**
     * @param ClrsApplication $app
     *
     * @return ResponseInterface
     */
    public function run(ClrsApplication $app): ResponseInterface
    {
        echo 'We are running the callback for the route matched!' . PHP_EOL;

        $callback = $this->getCallback();
        $controllerActionArguments = $this->getControllerActionArgs();

        return $callback(...$controllerActionArguments);
    }
}
