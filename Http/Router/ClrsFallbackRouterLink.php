<?php

namespace Celeritas\Http\Router;

use Celeritas\Application\ClrsApplication;
use Celeritas\Http\Response\Interfaces\ResponseInterface;

class ClrsFallbackRouterLink extends Abstracts\ClrsRouterLinkAbstract
{
    public function __construct(
        protected readonly string $controllerClass,
        protected readonly \Closure $controllerAction,
    ) {
    }

    /**
     * @param ClrsApplication $app
     *
     * @return ResponseInterface
     */
    public function run(ClrsApplication $app): ResponseInterface
    {
        echo PHP_EOL . 'We are running the Controlera fallback route!' . PHP_EOL;

        $controllerClass = $this->getController();

        $app->controllers = $app->singleton(
            class: $controllerClass,
            args: [
                $app,
            ]
        );

        $controllerAction = $this->getAction();
        $controllerActionArguments = $this->getControllerActionArgs();

        return $controllerAction(...$controllerActionArguments);
    }
}
