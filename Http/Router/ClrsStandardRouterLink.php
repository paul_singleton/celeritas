<?php

namespace Celeritas\Http\Router;

use Celeritas\Application\ClrsApplication;
use Celeritas\Http\Response\Interfaces\ResponseInterface;

class ClrsStandardRouterLink extends Abstracts\ClrsRouterLinkAbstract
{
    public function __construct(
        protected readonly ?string $httpMethod = null,
        protected readonly string $pattern,
        protected readonly string $controllerClass,
        protected readonly \Closure $controllerAction,
        protected readonly ?string $name = null,
        protected readonly ?array $hardcodedArgs = null,
    ) {
    }

    /**
     * @param ClrsApplication $app
     *
     * @return ResponseInterface
     */
    public function run(ClrsApplication $app): ResponseInterface
    {
        echo 'We are running the callback for the route matched!' . PHP_EOL;

        $controllerClass = $this->getController();

        $app->controllers = $app->singleton(
            class: $controllerClass,
            args: [
                $app,
            ]
        );

        $controllerAction = $this->getAction();
        $controllerActionArguments = $this->getControllerActionArgs();

        return $controllerAction(...$controllerActionArguments);
    }
}
