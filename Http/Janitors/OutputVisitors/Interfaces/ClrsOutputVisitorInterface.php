<?php

namespace Celeritas\Http\Janitors\OutputVisitors\Interfaces;

use Celeritas\Application\ClrsApplication;

interface ClrsOutputVisitorInterface
{
    /**
     * @param ClrsApplication $app
     *
     * @return void
     */
    public function validateResponse(ClrsApplication $app): void;
}
