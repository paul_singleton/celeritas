<?php

namespace Celeritas\Http\Janitors\InputVisitors\Interfaces;

use Celeritas\Application\ClrsApplication;

interface ClrsInputVisitorInterface
{
    /**
     * @param ClrsApplication $app
     *
     * @return void
     */
    public function validateRequest(ClrsApplication $app): void;
}
