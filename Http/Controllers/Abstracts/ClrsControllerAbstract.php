<?php

namespace Celeritas\Http\Controllers\Abstracts;

use Celeritas\Application\ClrsApplication;
use Celeritas\Http\Controllers\InputVisitors\Interfaces;

abstract class ClrsControllerAbstract
{
    public function __construct(
        protected readonly ClrsApplication $app,
    ) {
    }

    /**
     * Validatre only the controller specific request.
     *
     * @param Interfaces\ClrsControllerVisitorInterface $visitor
     *
     * @return void
     */
    public function acceptInputValidatorVisitor(Interfaces\ClrsControllerVisitorInterface $visitor): void
    {
        $visitor->validateControllerRequest($this->app);
    }
}
