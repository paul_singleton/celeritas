<?php

namespace Celeritas\Http\Controllers\InputVisitors\Interfaces;

use Celeritas\Application\ClrsApplication;

interface ClrsControllerVisitorInterface
{
    /**
     * @param ClrsApplication $app
     *
     * @return void
     */
    public function validateControllerRequest(ClrsApplication $app): void;
}
