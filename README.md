# Celeritas Cluster organisation

## Basic information

A so called "Celetitas Cluster" consists of one Celeritas Framework Instance and one or more Celeritas Projects, bind to it.

Each Celeritas Cluster is to be per one administrator - the persosn or owner of the cluster, that manages it.
It is not recommended that we have different projects inside in one Celeritas Cluster, that belong to different administrators.

Why?

Because, these different Administrators do not know each other. Who is going to controll or update the Celeritas Cluster?
If one of them updates the current Celeritas Cluster, and breaks its project(s), this will break the projects of the other Administrators too.

The idea behind the Celeritas Cluster is to be shared amongst many independant projects, but administered by one Administrator.

A kind of a "cluster administrator"? Bad idea.

Other possible idea is an Administrator to have multiple Celeritas Clusters, with one or multiple projects each,
thus giving the possibility for him to move its projects from one cluster to another.
In case, lets say, one of the user cluster is broken...

## GIT repository

Celeritas Framework Instance is to be GIT-cloned in a root Celeritas Cluster folder, for example:
`.../celeritas_cluster_1/...`

User level projects are to be also GIT maintained in a "projects" subfolder, per project each, with its GIT repo each.

## Celeritas Cluster folder structure
```
                                            |----> project_1_1
            |----> celeritas_cluster_1 ---> |----> project_1_2
            |                               |----> project_1_3
            |                               | ...
            |
            |                               |----> project_2_1
user1 ----> |----> celeritas_cluster_2 ---> |----> project_2_2
            |                               |----> project_2_3
            |                               | ...
            |
            |                               |----> project_3_1
            |----> celeritas_cluster_3 ---> |----> project_3_2
            |                               |----> project_3_3
            |                               | ...
            | ...
```

# Custom project folder structure
...


# Celeritas request lifecycle

We divide the entire request lifecycle in these phases:
1) Bootstrap phase
2) Input Janitors phase
3) Routing and Controller/Action phase
4) Output Janitors phase
5) Output phase

## 1. Bootstrap phase

First, what do we mean by "bootstrapping"?
We mean creating a basic ClrsApplication object `($app)`, and also - creating other objects, used as instruments
like logging, configuration etc...

They are attached as properties of the `$app` object, like:
`$app->configs`, `$app->loggers`, `$app->routers`...

These are to be used as the necessary minimum for the application to run.

Also, setting custom error, exception and shutdown handlers. These are not attached to $app like the above ones, they are set on PHP level.

The goal of this phase is to have a fully functional application, with all its basic "instrument objects" instantiated as `$app` properties.
Also - setting custom error, exception and shutdown handlers.

Some of these like Routers, are a must, but some like loggeers, configs... are not, but we just wont have such custom functionality
and thus leaving logging or exception handling for example, to the PHP.

### What is a Celeritas Bootstrapper actually?

It is a class that implements `ClrsBootstrapperInterface` with one method `bootMeUp(ClrsApplication $app): void`, that must be executed
in order to set the Celeritas ClrsApplication error, exception and shutdown handlers,
and to create and attach configs, routers and loggers objects to the `$app`.






НАПИШИ И ОНОВА, ЧЕ МОЖЕШ ДА ИМАШ РАЗЛИЧНИ ЕРРОР, ЕКЦЕПШЪН И ШЪТДАУН ХЕНДЛЪРИ, КОЙТО ДА ДЖУРКАЖ В ИНДЕКС.ПХП-ТО





### What would be a successful application bootstrap?

That would mean the successful creation of the abovementioned main Celeritas Application Object `ClrsApplication $app` and all its "instruments" 
and setting the error, exception and shutdown custom handlers.

### What if we have failure during the bootstrap process?

That would mean that we do not have a functioning Celeritas Application Object and the only thing we could do in this case,
is to notify the client for this with some simple system notification - be it a HTML page, JSON mesage...
and to try to log information about the situation, using a simple PHP script.

Crashing in this phase dispose us of the obligation to offer at least the minimal Celeritas funcionalities.
It's like "If you can't start your car to go to work, start your car ang go to a auto mobile mechanic..."
The idea here is to try to show some error information to the client and try to log some debug information.

That would be enough. And the only possible thing.

## 2. Input Janitors phase

So called `Input Janitors`, as the name suggests, a objects of classes, implementing common interface (ClrsInputVisitorInterface) build on the Visitor Design Pattern.
They get the `$app` object and preform global validations like for example - is this HTTP method allowed, is the client IP allowed etc...
Upon validation failure, an exceptions is thrown.

## 3. Routing and Controller/Action phase



## 4. Output Janitors phase



## 5. Output phase



# Celeritas Configuration Functionality

Configuration functionality in Celeritas is provided by the **ClrsApplicationConfigs** core package.
From programatical point of view it is a PHP class, whose object is set to the `$configs` property
of the `ClrsApplication` object (`$app->configs`).

The idea behind the **ClrsApplicationConfigs** object is to extract, contain and provide `system` and `configuration`
information needed for the Celeritas Web Application (ClrsApplication) to run.

Lets consider `system` information, the one taken from the Operational System
and the Webserver, be it Apache or Nginx.

Lets consider `configuration` information, the one set using the `.env` file (default one or per environment)
and the `.php` files in the configs/[environment]/` directories.

## Elements of the `ClrsApplicationConfigs` object

For the purpose of extracting, storing and giving access to the abovementioned `system` and `configuration`,
the `ClrsApplicationConfigs` class has couple of object properties
(`Envs $envs`, `Servers $servs`, `ClrsDotenvs $dotenvsMain`, `ClrsDotenvs $dotenvsSpecific` and `ClrsPhps $phps`),
each of them containing information as a readonly private array `$configsData`,
used from the `ClrsConfigsTrait` trait.

What kind of configuration information does each of them manage?

### ClrsEnvs

OS environment information, taken using the `getenv()` php function.
Such information can also be set dynamically, from the command line etc...

Nesting `/path/to/the/ = value` is possible and such paths will be exploded and turned
into a multidimensional array element of the `$configsData` array.

E.g. `/conf1/subconf2/subsubconf3/ = 123 Hello World` will be turned into
`$configsData[conf1][subconf2][subsubconf3] = '123 Hello World'`

Delimeter must be `/`

### ClrsServers

Configuration information taken from the `$_SERVER` superglobal array, containing webserver specific information.
Configuration values can be set dynamically in `.htaccess` file (Apache) or in the Nginx config file.

Nesting `/path/to/the/ = value` is possible and such paths will be exploded and turned
into an associative, multidimensional array element of the `$configsData` array
in a simmilar way as in the `Envs` object.

Delimeter must be `/`

### ClrsDotenvs

Configuration information is taken from a `.env` files. Why files, not file?
Because Celeritas supports multienviroment configurattion in a way you must have a default `.env` file,
but you may also have an environment specific version of the `.env` file, e.g. `.env.dev`, `.env.prod` etc...

And the configuration information there may override dhe default one in the `.env` file.

But the order of overriding will depend on the strategy we have set, will be explained later.

Nesting `/path/to/the/ = value` is possible and such paths will be exploded and turned
into an associative, multidimensional array element of the `$configsData` array
in a simmilar way as in the `Envs` object.

#### .env files syntax

The syntax of the `.env[.environment]` is simple and very similar to the one in frameworks like Lavarel.

`[/]CONF_NAME_1[/CONF_NAME_N][/] = value`

Lines can be commented with `#`.

Multiline values are not supported, one line - one configuration value.

### ClrsPhps

Configuration information is stored in `*.php` files, as associative, multidimensional arrays.

All of the configuration files are stored in the `configs/[environment]/` directories
and each of the `.php` files contains information about certain part of the Celeritas application.

No parsing or any other additional work is needed, these `.php` configuration files are just PHP required.

## Accessing `ClrsApplicationConfigs` Values

Each of the abovementioned property objects (`Envs $envs`, `Servers $servs`, `ClrsDotenvs $dotenvsMain`, `ClrsDotenvs $dotenvsSpecific`
and `ClrsPhps $phps`) has its own private, multidimensional, associative array, called `$configsData`, that can be accessed only 
through two public getter methods - `get(string, string|float|bool = null, array = null, bool = false): string|float|bool|null` 
and `all(string = null): array`.

### `all(string = null): array`

As the name suggests, this method returns all the entire `$configsData` and also giving you the possibility
to return only part of it by one of the root keys.

### `get(string, string|float|bool = null, array = null, bool = false): string|float|bool|null`

Gets as a parametter the `path/to/the/value` string, searches through the entire `$configsData`
and returns the configuration value wanted.
E.g. `$app->configs->envs->get('/database/mysql/user/');`

## Access all configuration and system information

It is also possible to combine all or some of the different ***ClrsApplicationConfigs*** elements information
combined and overriding each other by the key value, using different overriding strategies.

These so called `strategies` are build using Strategy Design Pattern.
Each of them can independently set a custom way to set the order of who overrides who
by the key of the given value.
E.g. if we have elements with a key `/database/mysql/user/` in both `Envs $envs` and `Servers $servs`
we may want that the latter overrides the first, or the contrary, we define this order of overriding
in different strategy.

We can acceess the combined `system` and `configuration` using: `$app->configs->all()`
and e.g. `$app->configs->get('/database/mysql/user/');`





# Celeritas Routing Functionality

We have a coming HTTP request, we want to easily switch different routers (PHP router classes) 
that can route that same request to the needed controller/action.

And lets emphasize on the `SAME request`.
What does it mean?
It means that one and the same HTTP request (e.g. http://somesite/one/two?three=3) must not be routed 
by only one router to a particular controller/action.

Rather, there will be a set of multiple routers (PHP router classes), each offering routing 
to a different controller/action for the given HTTP request.

Simply said - I want that this given request leads to that controller/action, but I also want to be able 
to switch to other router that will route the same request to other controller/action.

How? By simply use other PHP router class.
