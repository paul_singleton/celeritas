<?php

namespace Celeritas\Traits;

trait ClrsServiceContainerTrait
{
    private array $accessoriesClassPool = [];
    private array $accessoriesObjectsPool = [];

    /**
     * The logic to form the accessories class pool identificator.
     *
     * @param string $code|null
     * @param string $class|null
     *
     * @return string
     */
    private function accesoryClassKey(string $code = null, string $class = null): string
    {
        return empty($code) ? md5($class) : md5($code);
    }

    /**
     * Populate the class container array.
     *
     * @param string $code
     * @param string $class
     * @param array $args
     *
     * @return void
     */
    public function bindAccesoryClass(string $code, string $class, array $args = []): void
    {
        $objectPoolKey = $this->accesoryClassKey($code, $class);

        $this->accessoriesClassPool[$objectPoolKey] = [
            'class' => $class,
            'args' => $args
        ];
    }

    /**
     * @param string $code|null
     * @param string $class|null
     *
     * @return string|null
     */
    private function getAccesoryClass(string $code, string $class): ?string
    {
        $objectPoolKey = $this->accesoryClassKey($code, $class);

        return $this->accessoriesClassPool[$objectPoolKey] = $class;
    }

    /**
     * If we have already instantiated object before, just return it, do not instantiate new one.
     *
     * @param string|null $code
     * @param string|null $class
     * @param array $args
     *
     * @return object|null
     */
    public function singleton(string $code = null, string $class = null, array $args = []): ?object
    {
        $objectPoolKey = $this->accesoryClassKey($code, $class);

        $class = $this->accessoriesClassPool[$objectPoolKey]['class'] ?? $class;
        $args  = $this->accessoriesClassPool[$objectPoolKey]['args'] ?? $args;

        foreach ($this->accessoriesObjectsPool as $accessoryObject) {
            if ($accessoryObject instanceof $class) {
                return $accessoryObject;
            }
        }

        return $this->accessoriesObjectsPool[$objectPoolKey] = new $class(...$args);
    }

    /**
     * Produce a pristine, new object by simple "new" instantioation.
     *
     * @param string|null $code
     * @param string|null $class
     * @param array $args
     *
     * @return object|null
     */
    public function instance(string $code = null, string $class = null, array $args = []): ?object
    {
        $objectPoolKey = $this->accesoryClassKey($code, $class);

        $class = $this->accessoriesClassPool[$objectPoolKey]['class'] ?? $class;
        $args  = $this->accessoriesClassPool[$objectPoolKey]['args'] ?? $args;

        return $this->accessoriesObjectsPool[$objectPoolKey] = new $class(...$args);
    }

    /**
     * If we have already instantiated object before, just clone it, do not instantiate new one.
     *
     * @param string|null $code
     * @param string|null $class
     * @param array $args
     *
     * @return object|null
     */
    public function prototype(string $code = null, string $class = null, array $args = []): ?object
    {
        $objectPoolKey = $this->accesoryClassKey($code, $class);

        $class = $this->accessoriesClassPool[$objectPoolKey]['class'] ?? $class;
        $args  = $this->accessoriesClassPool[$objectPoolKey]['args'] ?? $args;

        foreach ($this->accessoriesObjectsPool as $accessoryObject) {
            if ($accessoryObject instanceof $class) {
                return clone $accessoryObject;
            }
        }

        return $this->accessoriesObjectsPool[$objectPoolKey] = new $class(...$args);
    }
}
