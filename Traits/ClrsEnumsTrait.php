<?php

namespace Celeritas\Traits;

trait ClrsEnumsTrait
{
    /**
     * It would be great if there would be a way to get an enum by its name,
     * but unfortunately, we must do it ourselves.
     *
     * @param array  $cases
     * @param string $name
     *
     * @return null|\BackedEnum|\UnitEnum
     */
    public function getEnumByName(array $cases, string $name): null|\BackedEnum|\UnitEnum
    {
        $enum = array_filter($cases, fn (mixed $case): bool => $case->name === $name);

        return array_pop($enum);
    }
}
