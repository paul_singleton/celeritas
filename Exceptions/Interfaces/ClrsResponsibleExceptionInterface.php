<?php

namespace Celeritas\Exceptions\Interfaces;

use Celeritas\Configs\Interfaces\ClrsApplicationConfigsInterface;
use Celeritas\Http\Response\Interfaces\ResponseInterface;

interface ClrsResponsibleExceptionInterface
{
    /**
     * @param ClrsApplicationConfigsInterface $configs
     *
     * @return ResponseInterface
     */
    public function getThrowableResponse(ClrsApplicationConfigsInterface $configs): ResponseInterface;
}
