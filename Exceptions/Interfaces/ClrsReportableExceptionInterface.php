<?php

namespace Celeritas\Exceptions\Interfaces;

use Celeritas\Loggers\ClrsApplicationLoggers;

interface ClrsReportableExceptionInterface
{
    /**
     * @param ClrsApplicationLoggers $logger
     *
     * @return void
     */
    public function logThrowable(ClrsApplicationLoggers $logger): void;
}
