<?php

namespace Celeritas\Configs;

class ClrsServers implements Interfaces\ClrsApplicationConfigsInterface
{
    use Traits\ClrsConfigsTrait;

    private const NOTATION_SEPARATOR = '/';

    public function __construct()
    {
        // Variables, set in the .htaccess, httpd-vhosts.conf and other simmilar webserver config files.
        // Also, information about the given HTTP request.
        $servs = $_SERVER;

        $servs = $this->keypathToNested($servs);
        $servs = $this->mergeRecursively($servs);

        $this->configsData = $servs;
    }
}
