<?php

namespace Celeritas\Configs\Enums;

/**
 * The application environments we are supporting.
 */
enum ClrsEnvsEnum: string
{
    case default = '../.env';
    case dev     = '../.env.dev';
    case prod    = '../.env.prod';

    /**
     * @return string
     */
    public function getFileRealpath(): string
    {
        return (string) realpath($this->value);
    }
}
