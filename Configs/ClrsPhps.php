<?php

namespace Celeritas\Configs;

class ClrsPhps implements Interfaces\ClrsApplicationConfigsInterface
{
    use Traits\ClrsConfigsTrait;

    private const NOTATION_SEPARATOR = '/';
    private const PATH_TO_ENVS = '../configs/environments/';

    /**
     * @param Enums\ClrsEnvsEnum $envEnum
     */
    public function __construct(Enums\ClrsEnvsEnum $envEnum)
    {
        // Variables, set in the PHP config files.
        $phps = [];
        $configDirectory = $this->getConfigsFolder(self::PATH_TO_ENVS . $envEnum->name);
        $scannedDirectory = array_diff(scandir($configDirectory), array('..', '.'));
        foreach ($scannedDirectory as $scannedFilename) {
            $filename = pathinfo($scannedFilename, PATHINFO_FILENAME);
            $phps[$filename] = require $configDirectory . DIRECTORY_SEPARATOR . $scannedFilename;
        }

        $this->configsData = $phps;
    }

    /**
     * @param string $configsFolder
     *
     * @return string
     */
    private function getConfigsFolder(string $configsFolder): string
    {
        $path = realpath($configsFolder);
        if (is_dir($path)) {
            return $path  . DIRECTORY_SEPARATOR;
        }

        throw new \Exception('Configs path not found or is invalid!');
    }
}
