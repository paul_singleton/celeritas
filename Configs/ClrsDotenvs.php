<?php

namespace Celeritas\Configs;

class ClrsDotenvs implements Interfaces\ClrsApplicationConfigsInterface
{
    use Traits\ClrsConfigsTrait;

    private const NOTATION_SEPARATOR = '/';
    private const COMMENT_SYMBOL = '#';
    private const SPLIT_SYMBOL = '=';

    /**
     * @param Enums\ClrsEnvsEnum $envEnum
     */
    public function __construct(Enums\ClrsEnvsEnum $envEnum)
    {
        $dotEnvFile = $envEnum->getFileRealpath();

        $dotenvs = [];

        // Do we have corresponding .env.[CLRTS_APP_ENV] or not?
        if (empty($dotEnvFile) || !is_readable($dotEnvFile)) {
            $this->configsData = $dotenvs;
            return;
        }

        $lines = file($dotEnvFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($lines as $line) {
            $line = trim($line);
            $line = trim($line, self::NOTATION_SEPARATOR);

            // If the first symbol is..., ingore the entire row as a comment
            if (strpos($line, self::COMMENT_SYMBOL) === 0) {
                continue;
            }

            // In case we have comment on the same line, after the config directive
            if (strpos(trim($line), self::COMMENT_SYMBOL) > 0) {
                list($name, $value) = explode(self::COMMENT_SYMBOL, $line, 2);
                $line = trim($name);
            }

            list($name, $value) = explode(self::SPLIT_SYMBOL, $line, 2);

            // Spaces, tabs and more than one notation separators are not allowed in the keys of the array
            $name = preg_replace('/\s+/', '', $name);
            $name = preg_replace('/\t+/', '', $name);
            $name = trim(trim(trim($name), self::NOTATION_SEPARATOR));
            $name = preg_replace(
                '/\\' . self::NOTATION_SEPARATOR . '\\' . self::NOTATION_SEPARATOR . '+/',
                self::NOTATION_SEPARATOR,
                $name
            );

            // Values of the array are trimmed and typecasted
            $value = trim($value);
            $value = $this->valueTypecast($value);

            $dotenvs[$name] = $value;
        }

        $dotenvs = $this->keypathToNested($dotenvs);
        $dotenvs = $this->mergeRecursively($dotenvs);

        $this->configsData = $dotenvs;
    }
}
