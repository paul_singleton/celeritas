<?php

namespace Celeritas\Configs;

class ClrsEnvs implements Interfaces\ClrsApplicationConfigsInterface
{
    use Traits\ClrsConfigsTrait;

    private const NOTATION_SEPARATOR = '/';

    public function __construct()
    {
        // No matter if "E" is present in the "variables_order" INI directive,
        // getenv() will fetch the ENV variable.
        $envs = getenv();

        $envs = $this->keypathToNested($envs);
        $envs = $this->mergeRecursively($envs);

        $this->configsData = $envs;
    }
}
