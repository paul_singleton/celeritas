<?php

namespace Celeritas\Configs;

use Celeritas\Configs\Strategies\{
    Environment,
    Combination,
};
use Celeritas\Configs\Interfaces\{
    ClrsEnvStrategyIntervace,
    ClrsCombineStrategyInterface
};

class ClrsApplicationConfigs implements Interfaces\ClrsApplicationConfigsInterface
{
    use Traits\ClrsConfigsTrait;

    private const NOTATION_SEPARATOR = '/';

    public readonly ClrsEnvs $envs;
    public readonly ClrsServers $servs;
    public readonly ClrsDotenvs $dotenvsMain;
    public readonly ClrsDotenvs $dotenvsSpecific;
    public readonly ClrsPhps $phps;

    public readonly Enums\ClrsEnvsEnum $environment;

    public function __construct(ClrsEnvStrategyIntervace $envStrategy, ClrsCombineStrategyInterface $combineStrategy)
    {
        $this->envs        = new ClrsEnvs();
        $this->servs       = new ClrsServers();
        $this->dotenvsMain = new ClrsDotenvs(Enums\ClrsEnvsEnum::default);

        // Strategy we use to determine the application environment
        $envStrategyContext = new Environment\ClrsEnvStrategyContext($envStrategy);
        $this->environment = $envStrategyContext->getEnvironment($this->envs, $this->servs, $this->dotenvsMain);

        // .env and .env.[dev/prod...] settings
        $this->dotenvsSpecific = new ClrsDotenvs($this->environment);

        // php files settings
        $this->phps = new ClrsPhps($this->environment);

        // Strategy we use to combine and override the envs and confs from above
        $confsCombineStrategyContext = new Combination\ClrsConfigsCombineStrategyContext($combineStrategy);
        $this->configsData = $confsCombineStrategyContext->getConfigs(
            $this->envs,
            $this->servs,
            $this->dotenvsMain,
            $this->dotenvsSpecific,
            $this->phps
        );
    }
}
