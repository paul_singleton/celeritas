<?php

namespace Celeritas\Configs\Invoked;

use Celeritas\Configs\Interfaces;

class ClrsFilesystemRoot implements Interfaces\ClrsInvokedInterface
{
    /**
     * @return string|null
     */
    public function __invoke(): ?string
    {
        if (!empty($_ENV['PWD'])) {
            return $_ENV['PWD'] . DIRECTORY_SEPARATOR;
        }

        if (!empty($_SERVER['DOCUMENT_ROOT'])) {
            return dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR;
        }

        return null;
    }
}
