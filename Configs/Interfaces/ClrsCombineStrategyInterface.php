<?php

namespace Celeritas\Configs\Interfaces;

use Celeritas\Configs\{
    ClrsEnvs,
    ClrsDotenvs,
    ClrsServers,
    ClrsPhps
};

interface ClrsCombineStrategyInterface
{
    /**
     * Combine all the envs and PHP confs, overriding them.
     *
     * @param ClrsEnvs $envs
     * @param ClrsServers $servers
     * @param ClrsDotenvs $dotenvsMain      The default .env values (in ".env" file)
     * @param ClrsDotenvs $dotenvsSpecific  The ones from the corresponding .env.[CLRTS_APP_ENV]
     * @param ClrsPhps $phps
     *
     * @return array
     */
    public function combineConfsNEnvs(
        ClrsEnvs $envs,
        ClrsServers $servers,
        ClrsDotenvs $dotenvsMain,
        ClrsDotenvs $dotenvsSpecific,
        ClrsPhps $phps
    ): array;
}
