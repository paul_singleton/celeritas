<?php

namespace Celeritas\Configs\Interfaces;

interface ClrsInvokedInterface
{
    /**
     * @return string
     */
    public function __invoke(): string|float|bool|null;
}
