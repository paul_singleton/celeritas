<?php

namespace Celeritas\Configs\Interfaces;

use Celeritas\Configs\{
    Enums\ClrsEnvsEnum,
    ClrsEnvs,
    ClrsDotenvs,
    ClrsServers,
};

interface ClrsEnvStrategyIntervace
{
    /**
     * Determine the application environment requires these three objects.
     *
     * @param ClrsEnvs $envs
     * @param ClrsServers $servers
     * @param ClrsDotenvs $dotenvsMain  The default .env values (in ".env" file)
     *
     * @return ClrsEnvsEnum
     */
    public function determineEnvironment(ClrsEnvs $envs, ClrsServers $servers, ClrsDotenvs $dotenvsMain): ClrsEnvsEnum;
}
