<?php

namespace Celeritas\Configs\Interfaces;

interface ClrsApplicationConfigsInterface
{
    /**
     * @param string                 $path
     * @param string|float|bool|null $defaultValue
     * @param array|null             $haystack
     * @param bool                   $caseSensitive
     *
     * @return string
     */
    public function get(string $path, string|float|bool $defaultValue = null, array $haystack = null, bool $caseSensitive = true): string|float|bool|null;

    /**
     * @return array
     */
    public function all(): array;
}
