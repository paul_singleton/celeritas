<?php

namespace Celeritas\Configs\Traits;

trait ClrsConfigsTrait
{
    private readonly array $configsData;

    /**
     * Turns this:
     * ["aaa/sss/ddd"] => string(11) "some test value"
     * into this:
     * ["aaa/sss/ddd"] => array(1) {
     *     ["aaa"] => array(1) {
     *         ["sss"] => array(1) {
     *             ["ddd"] => string(11) "some test value"
     *         }
     *     }
     * }
     *
     * @param array $arr
     *
     * @return array
     */
    public function keypathToNested(array $arr): array
    {
        array_walk($arr, function (string|array|float &$value, string $key): void {
            $key = trim($key, self::NOTATION_SEPARATOR);
            $explodedKey = explode(self::NOTATION_SEPARATOR, $key);
            if (count($explodedKey) > 1) {
                $explodedKey = array_reverse($explodedKey);
                $tmp = $value;
                foreach ($explodedKey as $key1 => $value1) {
                    $tmp = array($value1 => $tmp);
                }
                $value = $tmp;
            }
        });

        return $arr;
    }

    /**
     * Turns this:
     * array(2) {
     *    ["aaa/sss/ddd"] => array(1) {
     *        ["aaa"] => array(1) {
     *            ["sss"] => array(1) {
     *                ["ddd"] => string(11) "some test value"
     *            }
     *        }
     *    }
     *    ["aaa/sss/eee"]=>array(1) {
     *        ["aaa"]=>array(1) {
     *            ["sss"]=>array(1) {
     *                ["eee"]=>string(11) "other test value"
     *            }
     *        }
     *    }
     * }
     * into this:
     * array(1) {
     *    ["aaa"]=>array(1) {
     *        ["sss"]=>array(2) {
     *            ["eee"]=>string(11) "other test value"
     *            ["ddd"]=>string(11) "some test value"
     *        }
     *    }
     * }
     *
     * @param array $arr
     *
     * @return array
     */
    public function mergeRecursively(array $arr): array
    {
        $tempEnvs = [];
        foreach ($arr as $key => $value) {
            if (is_array($value)) {
                $tempEnvs = array_merge_recursive($value, $tempEnvs);
            } else {
                $tempEnvs[$key] = $value;
            }
        }

        return $tempEnvs;
    }

    /**
     * @param string|float $value
     *
     * @return string|float
     */
    public function valueTypecast(string|float $value): string|float
    {
        return match (true) {
            (strtolower($value) === 'true') => true,
            (strtolower($value) === 'false') => false,
            (strtolower($value) === 'null') => null,
            is_numeric($value) => $value * 1,
            default => $value,
        };
    }

    /**
     * Getter for given env, conf or combined value.
     *
     * @param string                 $path
     * @param string|float|bool|null $defaultValue
     * @param array|null             $haystack
     * @param bool                   $caseSensitive
     *
     * @return string
     */
    public function get(
        string $path,
        string|float|bool $defaultValue = null,
        array $haystack = null,
        bool $caseSensitive = false
    ): string|float|bool|null {
        if (empty($haystack)) {
            $haystack = $this->configsData;
        }

        // Spaces, tabs and more than one notation separators are not allowed in the keys of the array
        $path = preg_replace('/\s+/', '', $path);
        $path = preg_replace('/\t+/', '', $path);
        $path = trim(trim(trim($path), self::NOTATION_SEPARATOR));
        $path = preg_replace(
            '/\\' . self::NOTATION_SEPARATOR . '\\' . self::NOTATION_SEPARATOR . '+/',
            self::NOTATION_SEPARATOR,
            $path
        );

        $aPath = explode(self::NOTATION_SEPARATOR, $path);

        foreach ($aPath as $key => $value) {
            $value = trim(trim(trim($value), self::NOTATION_SEPARATOR));

            // If case doesn't matter - uppercase the haystick keys and the search key value
            if ($caseSensitive === false) {
                $haystack = array_change_key_case($haystack, CASE_UPPER);
                $value = strtoupper($value);
            }

            if (empty($haystack[$value])) {
                continue;
            }

            array_shift($aPath);

            if (is_array($haystack[$value])) {
                return $this->get(implode(self::NOTATION_SEPARATOR, $aPath), $defaultValue, $haystack[$value], $caseSensitive);
            }

            if (count($aPath) > 0) {
                break;
            }

            return $haystack[$value];
        }

        return $defaultValue;
    }

    /**
     * Getter for all env, conf or combined values.
     *
     * @param string|null $rootKey
     *
     * @return array
     */
    public function all(string $rootKey = null): array
    {
        return empty($rootKey) ? $this->configsData : $this->configsData[$rootKey];
    }
}
