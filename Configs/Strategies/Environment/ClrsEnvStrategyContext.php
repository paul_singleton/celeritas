<?php

namespace Celeritas\Configs\Strategies\Environment;

use Celeritas\Configs\{
    Interfaces\ClrsEnvStrategyIntervace,
    Enums\ClrsEnvsEnum,
    ClrsEnvs,
    ClrsDotenvs,
    ClrsServers,
};

class ClrsEnvStrategyContext
{
    /**
     * Set desired environment strategy in a private property.
     *
     * @param ClrsEnvStrategyIntervace $strategy
     */
    public function __construct(
        private readonly ClrsEnvStrategyIntervace $strategy
    ) {
    }

    /**
     * Calls the needed method of the given strategy.
     *
     * @param ClrsEnvs $envs
     * @param ClrsServers $servers
     * @param ClrsDotenvs $dotenvsMain
     *
     * @return ClrsEnvsEnum
     */
    public function getEnvironment(ClrsEnvs $envs, ClrsServers $servers, ClrsDotenvs $dotenvsMain): ClrsEnvsEnum
    {
        return $this->strategy->determineEnvironment($envs, $servers, $dotenvsMain);
    }
}
