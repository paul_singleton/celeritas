<?php

namespace Celeritas\Configs\Strategies\Environment;

use Celeritas\Configs\{
    Interfaces\ClrsEnvStrategyIntervace,
    Enums\ClrsEnvsEnum,
    ClrsEnvs,
    ClrsDotenvs,
    ClrsServers,
};
use Celeritas\Traits;

/**
 * One possible strategy to determine the applicatiopn environment (dev/prod...).
 * "default" must not be returned, because the name of the selected ClrsEnvsEnum
 * corresponds with "configs/..." folder.
 *
 * The overriding strategy is this (lowest are overriden by those above them):
 * 1. Set via the command prompt as OS envs (highest priority)
 * 2. Set on webserver level (apache conf, .htaccess, nginx)
 * 3. Set in .env.[CLRTS_APP_ENV] - .env.dev, .env.prod....
 * 4. Set in .env
 */
class ClrsEnvDefaultStrategy implements ClrsEnvStrategyIntervace
{
    use Traits\ClrsEnumsTrait;

    /**
     * @param ClrsEnvs $envs
     * @param ClrsServers $servers
     * @param ClrsDotenvs $dotenvsMain
     *
     * @return ClrsEnvsEnum
     */
    public function determineEnvironment(ClrsEnvs $envs, ClrsServers $servers, ClrsDotenvs $dotenvsMain): ClrsEnvsEnum
    {
        $environment = $envs->get('CLRTS_APP_ENV') ?? $servers->get('CLRTS_APP_ENV') ?? $dotenvsMain->get('CLRTS_APP_ENV');

        $environment
            or throw new \Exception('Environment could not be determined, please check your server, environment or configuration settings.');

        return $this->getEnumByName(ClrsEnvsEnum::cases(), $environment);
    }
}
