<?php

namespace Celeritas\Configs\Strategies\Combination;

use Celeritas\Configs\{
    Interfaces\ClrsCombineStrategyInterface,
    ClrsEnvs,
    ClrsDotenvs,
    ClrsServers,
    ClrsPhps
};

class ClrsConfigsCombineStrategyContext
{
    /**
     * Set desired confs and envs combination strategy in a private property.
     *
     * @param ClrsCombineStrategyInterface $strategy
     */
    public function __construct(
        private readonly ClrsCombineStrategyInterface $strategy
    ) {
    }

    /**
     * Calls the needed method of the given strategy.
     *
     * @param ClrsEnvs    $envs             The ones from OS or set in the command line
     * @param ClrsServers $servers          The ones from Apache, Nginx or .htaccess, httpd-vhosts.conf...
     * @param ClrsDotenvs $dotenvsMain      The default .env values (in ".env" file)
     * @param ClrsDotenvs $dotenvsSpecific  The ones from the corresponding .env.[CLRTS_APP_ENV]
     * @param ClrsPhps    $phps             The ones from the corresponding PHP config files
     *
     * @return array
     */
    public function getConfigs(
        ClrsEnvs $envs,
        ClrsServers $servers,
        ClrsDotenvs $dotenvsMain,
        ClrsDotenvs $dotenvsSpecific,
        ClrsPhps $phps
    ): array {
        return $this->strategy->combineConfsNEnvs(
            $envs,
            $servers,
            $dotenvsMain,
            $dotenvsSpecific,
            $phps
        );
    }
}
