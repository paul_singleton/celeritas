<?php

namespace Celeritas\Configs\Strategies\Combination;

use Celeritas\Configs\{
    Interfaces\ClrsCombineStrategyInterface,
    ClrsEnvs,
    ClrsDotenvs,
    ClrsServers,
    ClrsPhps
};

/**
 * One possible strategy to combine and override the applicatiopn configs.
 *
 * The overriding strategy is this (lowest are overriden by those above them):
 * 1. Set via the command prompt as OS envs (highest priority)
 * 2. Set on webserver level (apache conf, .htaccess, nginx)
 * 3. Set in .env.[CLRTS_APP_ENV] - .env.dev, .env.prod....
 * 4. Set in .env
 * 5. Set in the PHP config files (lowest priority)
 */
class ClrsConfigsDefaultCombineStrategy implements ClrsCombineStrategyInterface
{
    /**
     * @param ClrsEnvs    $envs             The ones from OS or set in the command line
     * @param ClrsServers $servers          The ones from Apache, Nginx or .htaccess, httpd-vhosts.conf...
     * @param ClrsDotenvs $dotenvsMain      The default .env values (in ".env" file)
     * @param ClrsDotenvs $dotenvsSpecific  The ones from the corresponding .env.[CLRTS_APP_ENV]
     * @param ClrsPhps    $phps             The ones from the corresponding PHP config files
     *
     * @return array
     */
    public function combineConfsNEnvs(
        ClrsEnvs $envs,
        ClrsServers $servers,
        ClrsDotenvs $dotenvsMain,
        ClrsDotenvs $dotenvsSpecific,
        ClrsPhps $phps
    ): array {
        // Get all those from OS or set in the command line
        $envsAll = $envs->all();

        // Get all those from Apache, Nginx or .htaccess, httpd-vhosts.conf
        // and other simmilar webserver config files
        $servsAll = $servers->all();

        // Get all those from the default .env values (in ".env" file)
        $dotenvsMainAll = $dotenvsMain->all();

        // Get all those from the corresponding .env.[CLRTS_APP_ENV]
        $dotenvsSpecificAll = $dotenvsSpecific->all();

        // Get all those from the corresponding PHP config files
        $phpsAll = $phps->all();

        // Overrive those from *.php with those from .env
        $combinedConfigs = array_replace_recursive($phpsAll, $dotenvsMainAll);

        // Overrive those above with those from .env.[CLRTS_APP_ENV]
        $combinedConfigs = array_replace_recursive($combinedConfigs, $dotenvsSpecificAll);

        // Overrive those above with those from Apache, Nginx or .htaccess, httpd-vhosts.conf...
        $combinedConfigs = array_replace_recursive($combinedConfigs, $servsAll);

        // Overrive those above with those from OS or set in the command line
        $combinedConfigs = array_replace_recursive($combinedConfigs, $envsAll);

        return $combinedConfigs;
    }
}
